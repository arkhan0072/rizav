<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index')->name('index');
Route::get('pricing', function(){
  return view('pricing');
});


Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/ads/data', 'AdminController@adsData')->name('admin.adsData');
    Route::get('/ads', 'AdminController@ad')->name('admin.ads');
    Route::get('/ads/active', 'AdminController@adActive')->name('admin.adActive');
    Route::get('/ads/suspend', 'AdminController@adSuspend')->name('admin.adSuspend');
    Route::get('/ads/featured', 'AdminController@Featured')->name('admin.featured');
    Route::get('/ads/unfeatured', 'AdminController@unFeatured')->name('admin.unfeatured');
    Route::get('/users/data', 'AdminController@userData')->name('admin.usersData');
    Route::get('/users', 'AdminController@user')->name('admin.users');
    Route::resource('/category', 'CategoryController');
    Route::put('/category', 'CategoryController@store');
    Route::get('/category/deleted/{id}', 'CategoryController@destroy')->name('delete');
    Route::resource('/pages', 'pageController');
    Route::resource('/states', 'StateController');
    Route::resource('/city', 'cityController');
    Route::resource('/faqs', 'FaqController');


});


Route::get('details', function(){
  return view('details');
});
Route::get('/faqs', 'homecontroller@faqindex');

Route::get('adpost', function(){
  return view('adpost');
});

Route::resource('ads', 'AdController');
Route::get('ads/select/category','AdController@selectCategory')->name('selectCategory');

Route::get('myads/{status}','HomeController@myAds');
Route::get('like/{ad}/{action}', 'AdController@likeIt')->name('like');
Route::get('giveMeCities', 'cityController@ajaxCities')->name('giveMeCities');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/{id}', 'HomeController@updateProfile')->name('updateprofile');