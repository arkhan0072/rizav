@extends('trade.master')
@section('content')
    <section id="main" class="clearfix details-page">
        <div class="container">
            <div class="breadcrumb-section">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">Electronics &amp; Gedget</a></li>
                    <li>Mobile Phone</li>
                </ol><!-- breadcrumb -->
                <h2 class="title">Mobile Phones</h2>
            </div>

            <div class="banner">

                <!-- banner-form -->
                <div class="banner-form banner-form-full">
                    <form action="#">
                        <!-- category-change
                        <div class="dropdown category-dropdown">
                            <a data-toggle="dropdown" href="#"><span class="change-text">Select Category</span> <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu category-change">
                                <li><a href="#">Fashion &amp; Beauty</a></li>
                                <li><a href="#">Cars &amp; Vehicles</a></li>
                                <li><a href="#">Electronics &amp; Gedgets</a></li>
                                <li><a href="#">Real Estate</a></li>
                                <li><a href="#">Sports &amp; Games</a></li>
                            </ul>
                        </div><!-- category-change -->

                        <!-- language-dropdown
                        <div class="dropdown category-dropdown language-dropdown ">
                            <a data-toggle="dropdown" href="#"><span class="change-text">United Kingdom</span> <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu  language-change">
                                <li><a href="#">United Kingdom</a></li>
                                <li><a href="#">United States</a></li>
                                <li><a href="#">China</a></li>
                                <li><a href="#">Russia</a></li>
                            </ul>
                        </div><!-- language-dropdown -->

                        <input type="text" name="keyword" class="form-control" placeholder="Type Your key word">
                        <button type="submit" class="form-control" value="Search">Search</button>
                    </form>
                </div><!-- banner-form -->
            </div>

            <div class="section slider">
                <div class="row">
                    <!-- carousel -->
                    <div class="col-md-7">
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @foreach($ad->images as $key => $img)
                                    <li data-target="#product-carousel" data-slide-to="{{$key}}"
                                        class="{{$key?'':'active'}}">
                                        <img src="{{asset('uploads/ads')}}/{{$img->name}}" alt="Carousel Thumb"
                                             class="img-responsive">
                                    </li>
                                @endforeach
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <!-- item -->
                                @foreach($ad->images as $key => $img)
                                    <div class="item {{$key?'':'active'}}">
                                        <div class="carousel-image">
                                            <!-- image-wrapper -->
                                            <img src="{{asset('uploads/ads')}}/{{$img->name}}" alt="Featured Image"
                                                 height="400px" width="620px">
                                        </div>
                                    </div><!-- item -->
                                @endforeach
                            </div><!-- carousel-inner -->

                            <!-- Controls -->
                            <a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
                                <i class="fa fa-chevron-right"></i>
                            </a><!-- Controls -->
                        </div>
                    </div><!-- Controls -->

                    <!-- slider-text -->
                    <div class="col-md-5">
                        <div class="slider-text">
                            <h2>{{$ad->amount}}</h2>
                            <h3 class="title">{{$ad->title}}</h3>
                            <p><span>Offered by: <a href="#">{{$ad->user->name}}</a></span>
                                <span> Ad ID:<a href="#"
                                                class="time">{{$ad->user_id}}{{$ad->category_id}}{{$ad->id}}</a></span>
                            </p>
                            <span class="icon"><i class="fa fa-clock-o"></i><a
                                        href="#">{{$ad->created_at->format('Y-m-d')}}</a></span>
                            <span class="icon"><i class="fa fa-map-marker"></i><a href="#">{{$ad->city->name}}
                                    ,{{$ad->city->state->name}}</a></span>
                            <span class="icon"><i
                                        class="fa 	{{$ad->user->is_dealer ? 'fa-suitcase' : 'fa-user'}} online"></i><a
                                        href="#">
											{{$ad->user->is_dealer ? 'Dealer' : 'Individual'}}
                                    <strong>(online)</strong></a></span>
                            <!-- short-info -->
                            <div class="short-info">
                                <h4>Short Info</h4>
                                <p><strong>Condition: </strong><a href="#">{{$ad->is_new ? 'NEW':'USED' }}</a></p>
                                <p><strong>Brand: </strong><a href="#">{{$ad->brand_name}}</a></p>
                                <p><strong>Views: </strong><a href="#">{{$ad->views_count}}</a></p>
                                <p><strong>Negotiable: </strong><a href="#">{{$ad->is_negotiable ? 'Yes':'No'}}</a></p>
                            </div><!-- short-info -->

                            <!-- contact-with -->
                            <div class="contact-with">
                                <h4>Contact with </h4>
                                <span class="btn btn-red show-number">
												<i class="fa fa-phone-square"></i>
												<span class="hide-text">Click to show phone number </span>
												<span class="hide-number">{{$ad->user->mobile}}</span>
											</span>
                                <!-- <a href="#" class="btn"><i class="fa fa-envelope-square"></i>Reply by email</a> -->
                            </div><!-- contact-with -->

                            <!-- social-links -->
                            <div class="social-links">

                                <p></p>
                                <p></p>
                                <p></p>
                                <p></p>
                                <p></p>

                                <h4>Share this ad</h4>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
                                </ul>
                            </div><!-- social-links -->
                        </div>
                    </div><!-- slider-text -->
                </div>
            </div>
            <div class="description-info">
                <div class="row">
                    <!-- description -->
                    <div class="col-md-8">
                        <div class="description">
                            <h4>Description</h4>
                            <p>{{$ad->description}}</p>
                        </div>
                    </div><!-- description -->

                    <!-- description-short-info -->
                    <div class="col-md-4">
                        <div class="short-info">
                            <h4>Short Info</h4>
                            <!-- social-icon -->
                            <ul>
                                <li><i class="fa fa-shopping-cart"></i><a href="#">Delivery: Meet in person</a></li>
                                <li><i class="fa fa-user-plus"></i><a href="#">More ads by
                                        <span>{{$ad->user->name}}</span></a></li>
                                <li><i class="fa fa-print"></i><a onclick="window.print();">Print this ad</a></li>
                                <li><i class="fa fa-reply"></i><a href="#">Send to a friend</a></li>
                                @if($ad->likedby()->where('ad_id',$ad->id)->count())

                                    <li><i style="color: red;" class="fa fa-heart"></i><a title="Dislike" href="{{route('like',[$ad->id,'dislike'])}}">Your Favorite Ad</a></li>
                                @else

                                    <li><i style="color: green;" class="fa fa-heart"></i><a title="Like It"  href="{{route('like',[$ad->id,'like'])}}">Save Your Favorit Ad</a></li>
                                @endif


                                <li><i class="fa fa-exclamation-triangle"></i><a href="#">Report this ad</a></li>
                            </ul><!-- social-icon -->
                        </div>
                    </div>
                </div><!-- row -->
            </div><!-- description-info -->

            <div class="recommended-info">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="section recommended-ads">
                            <div class="featured-top">
                                <h4>Recommended Ads for You</h4>
                            </div>
                            @foreach($related as $ad)
                                <div class="ad-item row">
                                    <!-- item-image -->
                                    <div class="item-image-box col-sm-4">
                                        <div class="item-image">
                                            <a href="{{route('ads.show',$ad->id)}}">
                                                <img src="{{asset('uploads/ads')}}/{{$ad->images[0]->name or 'default.png'}}"
                                                     alt="Image" height="150px" width="150px"></a>
                                        </div><!-- item-image -->
                                    </div>
                                    <!-- rending-text -->
                                    <div class="item-info col-sm-8">
                                        <!-- ad-info -->
                                        <div class="ad-info">
                                            <h3 class="item-price">{{$ad->amount}}</h3>
                                            <h4 class="item-title"><a
                                                        href="{{route('ads.show',$ad->id)}}">{{$ad->title}}</a></h4>
                                            <div class="item-cat">
                                                <span><a href="#">{{$ad->category->parentCategory->name}}</a></span> /
                                                <span><a href="#">{{$ad->category->name}}</a></span>
                                            </div>
                                        </div><!-- ad-info -->
                                        <!-- ad-meta -->
                                        <div class="ad-meta">
                                            <div class="meta-content">
                                                <span class="dated"><a href="#">{{$ad->created_at->toDateString()}}</a></span>
                                                <a href="#" class="tag"><i class="fa fa-tags"></i> {{$ad->is_new ? 'New':'Used'}}</a>
                                            </div>
                                            <!-- item-info-right -->
                                            <div class="user-option pull-right">
                                                <span class="visitors">Views: {{$ad->views_count}}</span>
                                                <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$ad->city->name}}, {{$ad->city->state->name}}"><i class="fa fa-map-marker"></i> </a>

                                                <a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$ad->user->is_dealer ? 'Dealer' : 'Individual'}}"><i class="fa {{$ad->user->is_dealer ? 'fa-suitcase' : 'fa-user'}}"></i> </a>
                                            </div><!-- item-info-right -->
                                        </div><!-- ad-meta -->
                                    </div><!-- item-info -->
                                </div><!-- ad-item -->
                            @endforeach
                        </div>
                    </div><!-- recommended-ads -->

                    @include('users.user_right_sidebar')
                </div><!-- row -->
            </div><!-- recommended-info -->
        </div><!-- container -->
    </section>

@stop
