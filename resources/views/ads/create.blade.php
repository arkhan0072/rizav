@extends('users.usermaster')
@section('user_content')

			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li>Ad Post</li>
				</ol><!-- breadcrumb -->
				<h2 class="title">Mobile Phones</h2>
			</div><!-- banner -->

			<div class="adpost-details">
				<div class="row">
					<div class="col-md-8">
						<form action="{{route('ads.store')}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
							<input type="hidden" name="category" value="{{$category->id}}">
							<fieldset>
								<div class="section postdetails">
									<h4>Sell an item or service <span class="pull-right">* Mandatory Fields</span></h4>
									<div class="form-group selected-product">
										<ul class="select-category list-inline">
											<li>
												<a href="ad-post.html">
													<span class="select">
														<i class="fa {{$category->parentCategory->fa_icon}}" style="font-size:28px; color: green !important;"></i>
													</span>

													{{$category->parentCategory->name}}
												</a>
											</li>

											<li class="active">
												<a href="#"><span class="select"><i class="fa {{$category->fa_icon}}" style="font-size:28px; color: green !important;"></i></span>  {{$category->name}} </a>
											</li>
										</ul>
										<a class="edit" href=""><i class="fa fa-pencil"></i>Edit</a>
									</div>
									<div class="row form-group {{ $errors->has('sellType') ? 'has-error': ''}}">
										<label class="col-sm-3">Type of ad<span class="required">*</span></label>
										<div class="col-sm-9 user-type">
											<input type="radio" required="required" name="sellType" value="1" id="newsell"> <label for="newsell">I want to sell </label>
											<input type="radio" required="required" name="sellType" value="0" id="newbuy"> <label for="newbuy">want to buy</label>
										</div>
                    @if($errors->has('sellType'))
                    <span class="help-block">
                <strong>{{ $errors->first('sellType') }}</strong>
              </span>
                    @endif
									</div>
									<div class="row form-group add-title {{ $errors->has('title') ? 'has-error': ''}}">
										<label class="col-sm-3 label-title">Title for your Ad<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" required="required" class="form-control" id="text" name="title" value="{{old('title')}}" placeholder="ex, Sony Xperia dual sim 100% brand new ">
                      @if($errors->has('title'))
                      <span class="help-block">
                  <strong>{{ $errors->first('title') }}</strong>
                </span>
                      @endif
                    </div>
									</div>

									<div class="row form-group add-image">
										<label class="col-sm-3 label-title">Photos for your ad <span>(This will be your cover photo )</span> </label>
										<div class="col-sm-9">
											<h5><i class="fa fa-upload" aria-hidden="true"></i>Select Files to Upload / Drag and Drop Files <span>You can add multiple images.</span></h5>
											<div class="upload-section">
												<label class="upload-image" for="upload-image-one">
													<input type="file" name="image[]" id="upload-image-one">
												</label>

												<label class="upload-image" for="upload-image-two">
													<input type="file" name="image[]" id="upload-image-two">
												</label>
												<label class="upload-image" for="upload-image-three">
													<input type="file" name="image[]" id="upload-image-three">
												</label>

												<label class="upload-image" for="upload-imagefour">
													<input type="file" name="image[]" id="upload-imagefour">
												</label>
											</div>
										</div>
									</div>
									<div class="row form-group {{$errors->has('itemCon') ? 'has-error': ''}} select-condition">
										<label class="col-sm-3">Condition<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="radio" required="required" name="itemCon" value="1" id="new">
											<label for="new">New</label>
											<input type="radio" required="required" name="itemCon" value="0" id="used">
											<label for="used">Used</label>
										</div>
                    @if($errors->has('itemCon'))
                    <span class="help-block">
                <strong>{{ $errors->first('itemCon') }}</strong>
              </span>
                    @endif
									</div>
									<div class="row form-group {{$errors->has('amount') ? 'has-error': ''}} select-price">
										<label class="col-sm-3 label-title">Price<span class="required">*</span></label>
										<div class="col-sm-9">
											<label>$USD</label>
											<input type="text" required="required" class="form-control" value="{{old('amount')}}" name="amount" id="text1">
                      @if($errors->has('amount'))
                      <span class="help-block">
                  <strong>{{ $errors->first('amount') }}</strong>
                </span>
                      @endif
											<input type="checkbox"  name="price" value="1" id="negotiable">
											<label for="negotiable">Negotiable </label>
										</div>
									</div>
									<div class="row form-group {{$errors->has('brandname') ? 'has-error': ''}} brand-name">
										<label class="col-sm-3 label-title">Brand Name<span class="required">*</span></label>
										<div class="col-sm-9">
                      @if($errors->has('brandname'))
                      <span class="help-block">
                  <strong>{{ $errors->first('brandname') }}</strong>
                </span>
                      @endif
											<input type="text" required="required" class="form-control" name="brandname" value="{{old('brandname')}}" placeholder="ex, Sony Xperia">
										</div>
									</div>




									<div class="row form-group {{$errors->has('description') ? 'has-error': ''}} item-description">
										<label class="col-sm-3 label-title">Description<span class="required">*</span></label>
										<div class="col-sm-9">

											<textarea class="form-control" name="description" id="textarea" placeholder="Write few lines about your products" rows="8">{{old('description')}}</textarea>
											@if($errors->has('description'))
												<span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
                </span>
											@endif
										</div>

									</div>
                  <div class="row form-group {{$errors->has('city') ? 'has-error': ''}} item-description">
                  										<label class="col-sm-3 label-title"><span class="required"></span></label>
                  										<div class="col-sm-9">
                                        <!-- section -->
                                        <select class="form-control" onchange="getCity(this.value);">
                                          <option value="">Select State</option>
                                          @foreach($states as $state)
                                          <option value="{{$state->id}}" >{{$state->name}}</option>
                                          @endforeach
                                        </select>
                                       <select class="form-control" name="city" id="city-list">
                                          <option value="">Select City</option>

                                          <script>
                                          function getCity(val) {
                                            $.ajax({
                                            type: "GET",
                                            url : "{{route('giveMeCities')}}",
                                            data:'id='+val,
                                            success: function(data){
                                              $("#city-list").html(data);
                                            }
                                            });
                                          }
                                          </script>
                                        </select>
                  										</div>

                  									</div>
								</div>



								<div class="checkbox section agreement">
									<label for="send">
										<input type="checkbox" name="send" id="send">
										 By clicking "Post", you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a> and acknowledge that you are the rightful owner of this item and using Trade to find a genuine buyer.
									</label>
									<button type="submit" class="btn btn-primary">Post Your Ad</button>
								</div><!-- section -->

							</fieldset>
						</form><!-- form -->
					</div>


					<!-- quick-rules -->
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Quick rules</h4>
							<p class="lead">Posting an ad on <a href="#">Trade.com</a> is free! However, all ads must follow our rules:</p>

							<ul>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
								<li>Do not put your email or phone numbers in the title or description.</li>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
							</ul>
						</div>
					</div><!-- quick-rules -->
				</div><!-- photos-ad -->
			</div>
		</div><!-- container -->

@stop
