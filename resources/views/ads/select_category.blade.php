@extends('trade.master')
@section('content')

<section id="main" class="clearfix ad-post-page">
		<div class="container">

			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="{{route('index')}}">Home</a></li>
					<li>Ad Post</li>
				</ol><!-- breadcrumb -->
				<h2 class="title">Post Free Ad</h2>
			</div><!-- banner -->



			<div id="ad-post">
				<div class="row category-tab">
					<div class="col-md-4 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Select a Category</h4>
							<ul role="tablist">
                @foreach ($categories as $value)
                <li><a href="#cat{{$value->id}}" aria-controls="cat{{$value->id}}" role="tab" data-toggle="tab">

					  <i class="fa {{$value->fa_icon}}" style="font-size:28px; color: green !important;"></i>

                  {{$value->name}}
                </a></li>
                @endforeach

							</ul>
						</div>
					</div>

					<!-- Tab panes -->
					<div class="col-md-4 col-sm-6">
						<div class="section tab-content subcategory post-option">
							<h4>Select a subcategory</h4>
@foreach ($categories as $value)
<div role="tabpanel" class="tab-pane" id="cat{{$value->id}}">
  <ul>
@foreach ($value->subCategories as $sub)
<li><a href="{{route('ads.create')}}?id={{$sub->id}}"><i class="fa {{$sub->fa_icon}}" style="font-size:28px; color: green !important;"></i> {{$sub->name}}
		</a></li>
@endforeach
</ul>
</div>
@endforeach


						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="section next-stap post-option">
							<h2>Post an Ad in just <span>30 seconds</span></h2>
							<p>Please DO NOT post multiple ads for the same items or service. All duplicate, spam and wrongly categorized ads will be deleted.</p>
							<div class="btn-section">
								<a href="ad-post-details.html" class="btn">Next</a>
								<a href="#" class="btn-info">or Cancle</a>
							</div>
						</div>
					</div><!-- next-stap -->
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<div class="ad-section">
							<a href="#"><img src="{{asset('images/ads/3.jpg')}}" alt="Image" class="img-responsive"></a>
						</div>
					</div>
				</div><!-- row -->
			</div>
		</div><!-- container -->
	</section>



@stop
