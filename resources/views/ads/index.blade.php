@extends('trade.master')
@section('content')

<section id="main" class="clearfix category-page main-categories">
		<div class="container">
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<!--<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li>Electronics &amp Gedget</li>
				</ol>--><!-- breadcrumb -->
				<h2 class="title">Mobile Phones</h2>
			</div>
			<div class="banner">

				<!-- banner-form -->
				<div class="banner-form banner-form-full">
					<form action="#">
						<!-- category-change
						<div class="dropdown category-dropdown">
							<a data-toggle="dropdown" href="#"><span class="change-text">Select Category</span> <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu category-change">
								<li><a href="#">Fashion &amp; Beauty</a></li>
								<li><a href="#">Cars &amp; Vehicles</a></li>
								<li><a href="#">Electronics &amp; Gedgets</a></li>
								<li><a href="#">Real Estate</a></li>
								<li><a href="#">Sports &amp; Games</a></li>
							</ul>
						</div><!-- category-change -->

						<!-- language-dropdown
						<div class="dropdown category-dropdown language-dropdown ">
							<a data-toggle="dropdown" href="#"><span class="change-text">United Kingdom</span> <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu  language-change">
								<li><a href="#">United Kingdom</a></li>
								<li><a href="#">United States</a></li>
								<li><a href="#">China</a></li>
								<li><a href="#">Russia</a></li>
							</ul>
						</div><!-- language-dropdown -->

						<input type="text" name="keyword" class="form-control" placeholder="Type Your key word">
						<button type="submit" class="form-control" value="Search">Search</button>
					</form>
				</div><!-- banner-form -->
			</div>

			<div class="category-info">
				<div class="row">
					<!-- accordion-->
					<div class="col-md-3 col-sm-4">
						<div class="accordion">
							<!-- panel-group -->
							<div class="panel-group" id="accordion">

								<!-- panel -->
								<div class="panel-default panel-faq">
									<!-- panel-heading -->
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#accordion-one" aria-expanded="false" class="collapsed">
											<h4 class="panel-title">All Categories<span class="pull-right"><i class="fa fa-plus"></i></span></h4>
										</a>
									</div><!-- panel-heading -->

									<div id="accordion-one" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
										<!-- panel-body -->
										<div class="panel-body">
											<ul>
												@foreach ($categories as $category)
												<li><a href="{{url()->current()}}?{{http_build_query($category)}}"><i class="icofont icofont-laptop-alt"></i>{{$category->name}}<span>({{$category->ads_count}})</span></a></li>
												@endforeach
											</ul>
										</div><!-- panel-body -->
									</div>
								</div><!-- panel -->

								<!-- panel -->
								<div class="panel-default panel-faq">
									<!-- panel-heading -->
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#accordion-two" class="collapsed" aria-expanded="false">
											<h4 class="panel-title">Condition<span class="pull-right"><i class="fa fa-plus"></i></span></h4>
										</a>
									</div><!-- panel-heading -->

									<div id="accordion-two" class="panel-collapse collapse" aria-expanded="false">
										<!-- panel-body -->
										<div class="panel-body">
											<label for="new"><input type="checkbox" name="new" id="new"> New</label>
											<label for="used"><input type="checkbox" name="used" id="used"> Used</label>
										</div><!-- panel-body -->
									</div>
								</div><!-- panel -->

								<!-- panel -->
							<!-- panel -->

								<!-- panel -->
								<div class="panel-default panel-faq">
									<!-- panel-heading -->
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#accordion-four" class="collapsed" aria-expanded="false">
											<h4 class="panel-title">
											Posted By
											<span class="pull-right"><i class="fa fa-plus"></i></span>
											</h4>
										</a>
									</div><!-- panel-heading -->

									<div id="accordion-four" class="panel-collapse collapse" aria-expanded="false">
										<!-- panel-body -->
										<div class="panel-body">
											<label for="individual"><input type="checkbox" name="individual" id="individual"> Individual</label>
											<label for="dealer"><input type="checkbox" name="dealer" id="dealer"> Dealer</label>

										</div><!-- panel-body -->
									</div>
								</div><!-- panel -->

								<!-- panel -->
								<!-- panel -->
							 </div><!-- panel-group -->
						</div>
					</div><!-- accordion-->

					<!-- recommended-ads -->
					<div class="col-sm-8 col-md-7">
						<div class="section recommended-ads">
							<!-- featured-top -->
							<div class="featured-top">
								<h4>Recommended Ads for You</h4>
								<div class="dropdown pull-right">

								<!-- category-change -->
								<div class="dropdown category-dropdown">
									<h5>Sort by:</h5>
									<a data-toggle="dropdown" href="#"><span class="change-text">Popular</span><i class="fa fa-caret-square-o-down"></i></a>
									<ul class="dropdown-menu category-change">
										<li><a href="#">Featured</a></li>
										<li><a href="#">Newest</a></li>
										<li><a href="#">All</a></li>
										<li><a href="#">Bestselling</a></li>
									</ul>
								</div><!-- category-change -->
								</div>
							</div><!-- featured-top -->
						@foreach($ads as $ad)
							<!-- ad-item -->
							<div class="ad-item row">
								<!-- item-image -->
								<div class="item-image-box col-sm-4">
									<div class="item-image">
										<a href="{{route('ads.show',$ad->id)}}"><img src="{{asset('uploads/ads')}}/
										@if(isset($ad->images[0]->name))
											{{$ad->images[0]->name}}
											@else
													default.png
@endif
													"
																					 alt="Image"  height="150px" width="150px"></a>
									</div><!-- item-image -->
								</div>

								<!-- rending-text -->
								<div class="item-info col-sm-8">
									<!-- ad-info -->
									<div class="ad-info">
										<h3 class="item-price">{{$ad->amount}}</h3>
										<h4 class="item-title"><a href="{{route('ads.show',$ad->id)}}">{{$ad->title}}</a></h4>
										<div class="item-cat">
											<span><a href="#">{{$ad->category->parentCategory->name}}</a></span> /
											<span><a href="#">{{$ad->category->name}}</a></span>
										</div>
									</div><!-- ad-info -->

									<!-- ad-meta -->
									<div class="ad-meta">
										<div class="meta-content">
											<span class="dated"><a href="#">{{$ad->created_at->toDateString()}}</a></span>
											<a href="#" class="tag"><i class="fa fa-tags"></i> {{$ad->is_new ? 'New':'Used'}}</a>
										</div>
										<!-- item-info-right -->
										<div class="user-option pull-right">
											<span class="visitors">Views: {{$ad->views_count}}</span>
											<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$ad->city->name}}, {{$ad->city->state->name}}"><i class="fa fa-map-marker"></i> </a>

											<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$ad->user->is_dealer ? 'Dealer' : 'Individual'}}"><i class="fa {{$ad->user->is_dealer ? 'fa-suitcase' : 'fa-user'}}"></i> </a>
										</div><!-- item-info-right -->
									</div><!-- ad-meta -->
								</div><!-- item-info -->
							</div><!-- ad-item -->
							@endforeach



							<!-- pagination  -->

							<div class="text-center">
								{{ $ads->links() }}

							</div><!-- pagination  -->
						</div>
					</div><!-- recommended-ads -->

					<div class="col-md-2 hidden-xs hidden-sm">
						<div class="advertisement text-center">
							<a href="#"><img src="{{asset('images/ads/2.jpg')}}" alt="" class="img-responsive"></a>
						</div>
					</div>
				</div>
			</div>
		</div><!-- container -->
	</section>


@stop
