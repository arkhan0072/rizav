@extends('trade.master')
@section('content')
<section id="main" class="clearfix page">
    <div class="container">
        <div class="faq-page">
            <div class="breadcrumb-section">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li>FAQ</li>
                </ol><!-- breadcrumb -->
                <h2 class="title">Trade ads FAQ</h2>
            </div>

            <div class="accordion">
                <div class="panel-group" id="accordion">

                    <div class="panel panel-default panel-faq">
                        @foreach($faqs as $faq)
                        <div class="panel-heading active-faq">
                            <a data-toggle="collapse" data-parent="#accordion" href="#faq-one">
                                <h4 class="panel-title">
                                    {{$faq->title}}
                                    <span class="pull-right"><i class="fa fa-minus"></i></span>
                                </h4>
                            </a>
                        </div><!-- panel-heading -->

                        <div id="faq-one" class="panel-collapse collapse collapse in">
                            <div class="panel-body">
                                <p>{{$faq->content}}</p>
                            </div>
                        </div>
                    </div><!-- panel -->

@endforeach
                </div>
            </div>

        </div><!-- faq-page -->
    </div><!-- container -->
    <section id="something-sell" class="clearfix parallax-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="title">Did Not find your answer yet? Still need help ?</h2>
                    <h4>Send us a note to Help Center</h4>
                    <a href="contact-us.html" class="btn btn-primary">Contact Us</a>
                </div>
            </div><!-- row -->
        </div><!-- contaioner -->
    </section><!-- something-sell -->
</section>
    @stop