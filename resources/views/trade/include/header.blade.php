<!-- header -->
<header id="header" class="clearfix">
  <!-- navbar -->
  <nav class="navbar navbar-default">
    <div class="container">
      <!-- navbar-header -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{route('index')}}"><img class="img-responsive" src="{{asset('images/logo.png')}}" alt="Logo"></a>
      </div>
      <!-- /navbar-header -->

      <div class="navbar-left">
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active dropdown"><a href="{{route('index')}}" >Home </a>
            </li>
            <li><a href="{{route('ads.index')}}">Category</a></li>
            <li><a href="{{route('ads.index')}}">all ads</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>

      <!-- nav-right -->
      <div class="nav-right">
        <!-- language-dropdown -->
        <div class="dropdown language-dropdown">
          <i class="fa fa-globe"></i>
          <a data-toggle="dropdown" href="#"><span class="change-text">United Kingdom</span> <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu language-change">
            <li><a href="#">United Kingdom</a></li>
            <li><a href="#">United States</a></li>
            <li><a href="#">China</a></li>
            <li><a href="#">Russia</a></li>
          </ul>
        </div><!-- language-dropdown -->

        <!-- sign-in -->
        @auth
        <ul class="sign-in">

            <li><i class="fa fa-user"></i></li>
          <li><a href="{{route('home')}}"><b>My Account</b></a>
            @role('admin')
            <br>
            <li><i class="fa fa-user"></i></li>
              <a href="{{route('admin.index')}}"><b>Dashboard</b></a>
              @endrole
          </li>
        </ul>
        @else
        <ul class="sign-in">
          <li><i class="fa fa-user"></i></li>
          <li><a href="{{route('login')}}"> Sign In </a></li>
          <li><a href="{{route('register')}}">Register</a></li>
        </ul>
        @endauth<!-- sign-in -->

        <a href="{{route('selectCategory')}}" class="btn">Post Your Ad</a>
      </div>
      <!-- nav-right -->
    </div><!-- container -->
  </nav><!-- navbar -->
</header><!-- header -->
