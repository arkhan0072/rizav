@extends('trade.master')
@section('content')
<section id="main" class="clearfix contact-us">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Pricing Table</li>
			</ol>
			<h2 class="title">Pricing Table</h2>

			<div class="pricing-section">
				<div class="row">
					<div class="col-sm-4">
						<div class="pric">
							<h2>Personal</h2>
							<h3><sup>$</sup>59<span>/Per Month</span></h3>
							<div class="pric-menu">
								<ul>
									<li><i class="fa fa-check" aria-hidden="true"></i>Product Galleries, and Blogs.</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Powerful Website Analytics</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Fully Integrated E-Commerce</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Sell Unlimited Products</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Mobile-Optimized Website</li>
									<li><i class="fa fa-times" aria-hidden="true"></i>Free Custom Domain</li>
									<li><i class="fa fa-times" aria-hidden="true"></i>24/7 Customer Support</li>
									<li><i class="fa fa-times" aria-hidden="true"></i>3% Sales Transaction Fee</li>
								</ul>
								<a href="#" class="btn btn-primary">Buy Now</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="pric active">
							<div class="popular">
								<h2>Most Popular</h2>
							</div>
							<h2>Business</h2>
							<h3><sup>$</sup>99<span>/Per Month</span></h3>
							<div class="pric-menu">
								<ul>
									<li><i class="fa fa-check" aria-hidden="true"></i>Product Galleries, and Blogs.</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Powerful Website Analytics</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Fully Integrated E-Commerce</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Sell Unlimited Products</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Mobile-Optimized Website</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Free Custom Domain</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>24/7 Customer Support</li>
									<li><i class="fa fa-times" aria-hidden="true"></i>3% Sales Transaction Fee</li>
								</ul>
								<a href="#" class="btn btn-primary">Buy Now</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="pric">
							<h2>Personal</h2>
							<h3><sup>$</sup>199<span>/Per Month</span></h3>
							<div class="pric-menu">
								<ul>
									<li><i class="fa fa-check" aria-hidden="true"></i>Product Galleries, and Blogs.</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Powerful Website Analytics</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Fully Integrated E-Commerce</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Sell Unlimited Products</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Mobile-Optimized Website</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>Free Custom Domain</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>24/7 Customer Support</li>
									<li><i class="fa fa-check" aria-hidden="true"></i>3% Sales Transaction Fee</li>
								</ul>
								<a href="#" class="btn btn-primary">Buy Now</a>
							</div>
						</div>
					</div>

				</div><!-- row -->
			</div><!-- pricing section -->
		</div><!-- container -->
	</section>
@stop
