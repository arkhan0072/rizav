@extends('trade.master')
@section('content')
<section id="main" class="clearfix ad-post-page">
		<div class="container">

			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li>Ad Post</li>
				</ol><!-- breadcrumb -->
				<h2 class="title">Post Free Ad</h2>
			</div><!-- banner -->



			<div id="ad-post">
				<div class="row category-tab">
					<div class="col-md-4 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Select a subcategory</h4>
							<ul role="tablist">
								<li><a href="#cat1" aria-controls="cat1" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/1.png" alt="Images" class="img-">
									</span>
									Cars &amp; Vehicles
								</a></li>

								<li class="active link-active"><a href="#cat2" aria-controls="cat2" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/2.png" alt="Images" class="img-responsive">
									</span>
									Electronics &amp; Gedgets
								</a></li>

								<li><a href="#cat3" aria-controls="cat3" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/3.png" alt="Images" class="img-responsive">
									</span>
									Real Estate
								</a></li>

								<li><a href="#cat4" aria-controls="cat4" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/4.png" alt="Images" class="img-responsive">
									</span>
									Sports &amp; responsive
								</a></li>

								<li><a href="#cat5" aria-controls="cat5" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/5.png" alt="Images" class="img-responsive">
									</span>
									Fshion &amp; Beauty
								</a></li>

								<li><a href="#cat6" aria-controls="cat6" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/6.png" alt="Images" class="img-responsive">
									</span>
									Pets &amp; Animals
								</a></li>

								<li><a href="#cat7" aria-controls="cat7" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/7.png" alt="Images" class="img-responsive">
									</span>
									Job Openings
								</a></li>

								<li><a href="#cat8" aria-controls="cat8" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/9.png" alt="Images" class="img-responsive">
									</span>
									Home Appliances
								</a></li>

								<li><a href="#cat9" aria-controls="cat9" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/10.png" alt="Images" class="img-responsive">
									</span>
									Matrimony Services
								</a></li>

								<li><a href="#cat10" aria-controls="home10" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/11.png" alt="Images" class="img-responsive">
									</span>
									Music &amp; Arts
								</a></li>

								<li><a href="#cat11" aria-controls="cat11" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/12.png" alt="Images" class="img-responsive">
									</span>
									Miscellaneous
								</a></li>

								<li><a href="#cat12" aria-controls="cat12" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/8.png" alt="Images" class="img-responsive">
									</span>
									Books &amp; Magazines
								</a></li>
							</ul>
						</div>
					</div>

					<!-- Tab panes -->
					<div class="col-md-4 col-sm-6">
						<div class="section tab-content subcategory post-option">
							<h4>Select a subcategory</h4>
							<div role="tabpanel" class="tab-pane" id="cat1">
								<ul>
									<li><a href="javascript:void(0)">Cars &amp; Buses</a></li>
									<li><a href="javascript:void(0)">Motorbikes &amp; Scooters</a></li>
									<li><a href="javascript:void(0)">Bicycles and Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Trucks, Vans &amp; Buses</a></li>
									<li><a href="javascript:void(0)">Tractors &amp; Heavy-Duty</a></li>
									<li><a href="javascript:void(0)">Auto Parts &amp; Accessories</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane active" id="cat2">
								<ul>
									<li><a href="javascript:void(0)">Laptop &amp; Computer</a></li>
									<li><a href="javascript:void(0)">Mobile Phones</a></li>
									<li><a href="javascript:void(0)">Phablet &amp; Tablets</a></li>
									<li><a href="javascript:void(0)">Audio &amp; MP</a></li>
									<li><a href="javascript:void(0)">Accessories</a></li>
									<li><a href="javascript:void(0)">Cameras</a></li>
									<li><a href="javascript:void(0)">Mobile Accessories</a></li>
									<li><a href="javascript:void(0)">TV &amp; Video</a></li>
									<li><a href="javascript:void(0)">Other Electronics</a></li>
									<li><a href="javascript:void(0)">TV &amp; Video Accessories</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat3">
								<ul>
									<li><a href="javascript:void(0)">Houses &amp; Plots</a></li>
									<li><a href="javascript:void(0)">Lands &amp; property</a></li>
									<li><a href="javascript:void(0)">Plots &amp; Lands</a></li>
									<li><a href="javascript:void(0)">Apartment</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat4">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat5">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat6">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat7">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat8">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat9">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat10">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat11">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="section next-stap post-option">
							<h2>Post an Ad in just <span>30 seconds</span></h2>
							<p>Please DO NOT post multiple ads for the same items or service. All duplicate, spam and wrongly categorized ads will be deleted.</p>
							<div class="btn-section">
								<a href="ad-post-details.html" class="btn">Next</a>
								<a href="#" class="btn-info">or Cancle</a>
							</div>
						</div>
					</div><!-- next-stap -->
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<div class="ad-section">
							<a href="#"><img src="images/ads/3.jpg" alt="Image" class="img-responsive"></a>
						</div>
					</div>
				</div><!-- row -->
			</div>
		</div><!-- container -->
	</section>
  @stop
