
@extends('trade.master')
@section('content')
<!-- signin-page -->
<section id="main" class="clearfix user-page">
  <div class="container">
    <div class="row text-center">
      <!-- user-login -->
      <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="user-account">
          <h2>User Login</h2>
          <!-- form -->
          <form action="#" method="post">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('email') ? 'has-error': ''}}">
              <input type="text" class="form-control" name="email" placeholder="email" >
              @if($errors->has('email'))
              <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? 'has-error': ''}}">
              <input type="password" class="form-control" name="password" placeholder="Password" >
              @if($errors->has('password'))
              <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
              @endif
            </div>
            <button type="submit" href="#" class="btn">Login</button>
          </form><!-- form -->

          <!-- forgot-password -->
          <div class="user-option">
            <div class="checkbox pull-left">
              <label for="logged"><input type="checkbox" name="logged" id="logged"> Keep me logged in </label>
            </div>
            <div class="pull-right forgot-password">
              <a href="{{'password/reset'}}">Forgot password</a>
            </div>
          </div><!-- forgot-password -->
        </div>
        <a href="{{route('register')}}" class="btn-primary">Create a New Account</a>
      </div><!-- user-login -->
    </div><!-- row -->
  </div><!-- container -->
</section><!-- signin-page -->
@stop
