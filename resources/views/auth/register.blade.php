<<<<<<< HEAD
@extends('trade.master')
@section('content')
<!-- signup-page -->
<section id="main" class="clearfix user-page">
  <div class="container">
    <div class="row text-center">
      <!-- user-login -->
      <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="user-account">
          <h2>Create a Trade Account</h2>
          <form action="#" method="post">
            {{csrf_field()}}
            <div class="form-group{{ $errors->has('name') ? 'has-error': ''}}">
              <input type="text" name="name" class="form-control" placeholder="Name" required autofocus>
              @if($errors->has('name'))
              <span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? 'has-error': ''}}">
              @if($errors->has('email'))
              <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
              @endif
              <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email Id">

            </div>
            <div class="form-group{{ $errors->has('password') ? 'has-error': ''}}">
              @if($errors->has('password'))
              <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
              @endif
              <input type="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Password">
            </div>
            <div class="form-group">
              <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
            </div>
            <div class="form-group{{ $errors->has('mobile') ? 'has-error': ''}}">
              @if($errors->has('mobile'))
              <span class="help-block">
          <strong>{{ $errors->first('mobile') }}</strong>
        </span>
              @endif
              <input type="text" name="mobile" class="form-control" value="{{old('mobile')}}" placeholder="Mobile Number">
            </div>
            <div class="form-group{{ $errors->has('company') ? 'has-error': ''}}">
              @if($errors->has('company'))
              <span class="help-block">
          <strong>{{ $errors->first('company') }}</strong>
        </span>
              @endif
              <input type="text" name="company" class="form-control" value="{{old('company')}}" placeholder="company">
            </div>
            <!-- select -->
            <select class="form-control{{ $errors->has('account') ? 'has-error': ''}}" name="account">
              @if($errors->has('account'))
              <span class="help-block">
          <strong>{{ $errors->first('account') }}</strong>
        </span>
              @endif
              <option value="">Account Type</option>
              <option value="1">Dealer</option>
              <option value="0">Individual</option>

            </select><!-- select -->
            <!--<select class="form-control" onchange="getCity(this.value);">
              <option value="">Select State</option>
              @foreach($states as $state)
              <option value="{{$state->id}}" >{{$state->name}}</option>
              @endforeach
            </select>
           <select class="form-control" id="city-list">
              <option value="">Select City</option>

              <script>
              function getCity(val) {
              	$.ajax({
              	type: "GET",
                url : "giveMeCities",
              	data:'id='+val,
              	success: function(data){
              		$("#city-list").html(data);
              	}
              	});
              }
              </script>
            </select>-->
            <div class="checkbox">
              <label class="pull-left checked" for="signing"><input type="checkbox" name="signing" id="signing"> By signing up for an account you agree to our Terms and Conditions </label>
            </div><!-- checkbox -->
            <button type="submit" href="#" class="btn">Registration</button>
          </form>
          <!-- checkbox -->

        </div>
      </div><!-- user-login -->
    </div><!-- row -->
  </div><!-- container -->
</section><!-- signup-page -->
@stop
=======
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
>>>>>>> 36868af14e9ef933a93571cb4ab56dd5fe4d9f62
