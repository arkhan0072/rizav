@extends('users.usermaster')
@section('user_content')

			<div class="profile">
				<div class="row">
					<div class="col-sm-8">
						<div class="my-ads section">
							<h2>My ads</h2>
							<!-- ad-item -->
							@foreach($ads as $ad)
							<div class="ad-item row">
								<!-- item-image -->
								<div class="item-image-box col-sm-4">
									<a href="{{route('ads.show',$ad->id)}}"><img src="{{asset('uploads/ads')}}/{{$ad->images[0]->name or 'default.png'}}" alt="Image"  class="img-responsive"></a>
									<div class="item-image">
									</div><!-- item-image -->
								</div>
								<!-- rending-text -->
								<div class="item-info col-sm-8">
									<!-- ad-info -->
									<div class="ad-info">
										<h3 class="item-price">{{$ad->amount}}</h3>
										<h4 class="item-title"><a href="{{route('ads.show',$ad->id)}}">{{$ad->title}}</a></h4>
										<div class="item-cat">
											<span><a href="#">{{$ad->category->parentCategory->name}}</a></span> /
											<span><a href="#">{{$ad->category->name}}</a></span>
										</div>
									</div><!-- ad-info -->
									<!-- ad-meta -->
									<div class="ad-meta">
										<div class="meta-content">
											<span class="dated">Posted On: <a href="#"> {{$ad->created_at}}</a></span>
											<span class="visitors">Views: {{$ad->views_count}}</span>
										</div>
										<!-- item-info-right -->
										<div class="user-option pull-right">
											<a class="edit-item" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit this ad"><i class="fa fa-pencil"></i></a>
											<a class="delete-item" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete this ad"><i class="fa fa-times"></i></a>
										</div><!-- item-info-right -->
									</div><!-- ad-meta -->
								</div><!-- item-info -->
							</div><!-- ad-item -->
							@endforeach
							<div class="text-center">
              {{ $ads->links() }}
							</div>
						</div>
					</div>
					@include('users.user_right_sidebar')
				</div><!-- row -->
			</div>
		</div><!-- container -->
@stop
