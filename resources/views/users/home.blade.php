@extends('users.usermaster')
@section('user_content')
	@if (\Session::has('baby'))
		<div class="alert alert-success">
			<ul>
				<li>{!! \Session::get('baby') !!}</li>
			</ul>
		</div>
	@endif

			<div class="profile">
				<div class="row">
					<div class="col-sm-8">
						<div class="user-pro-section">
							<!-- profile-details -->
							<div class="profile-details section">
								<h2>Profile Details</h2>
								<!-- form -->
								<form action="{{route('updateprofile',$user->id)}}" method="post">
									@csrf
                                    @php
                                    print_r($errors);
                                    @endphp
						<div class="form-group{{ $errors->has('name') ? 'has-error': ''}}">
									<label>Username</label>
									<input type="text" name="name" class="form-control" style="color:black;" value="{{old('name',$user->name)}}">
                                    @if($errors->has('name'))
                                        <span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
                                    @endif
								</div>
								<div class="form-group{{ $errors->has('email') ? 'has-error': ''}}">
									<label>Email ID</label>
									<input type="email" name="email" class="form-control" value="{{old('email',$user->email)}}">
                                    @if($errors->has('email'))
                                        <span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
                                    @endif
								</div>

								<div class="form-group{{ $errors->has('mobile') ? 'has-error': ''}}">
									<label for="name-three">Mobile</label>
									<input type="text" name="mobile" class="form-control" value="{{old('mobile',$user->mobile)}}">
                                    @if($errors->has('mobile'))
                                        <span class="help-block">
          <strong>{{ $errors->first('mobile') }}</strong>
        </span>
                                    @endif
								</div>
									<div class="form-group{{ $errors->has('company') ? 'has-error': ''}}">

											<label for="name-three">Company</label>
										<input type="text" name="company" class="form-control" value="{{old('company',$user->company)}}" placeholder="company">
                                        @if($errors->has('company'))
                                            <span class="help-block">
          <strong>{{ $errors->first('company') }}</strong>
        </span>
                                        @endif
									</div>
								<!--<div class="form-group">
									<label>Your City</label>
									<select class="form-control">
										<option value="#">Los Angeles, USA</option>
										<option value="#">Dhaka, BD</option>
										<option value="#">Shanghai</option>
										<option value="#">Karachi</option>
										<option value="#">Beijing</option>
										<option value="#">Lagos</option>
										<option value="#">Delhi</option>
										<option value="#">Tianjin</option>
										<option value="#">Rio de Janeiro</option>
									</select>
								</div>-->

								<div class="form-group">
									<label>You are a</label>
									<select class="form-control" name="is_dealer">
										<option value="1" {{$user->is_dealer ?'selected=selected':''}}>Dealer</option>
										<option value="0" {{!$user->is_dealer ?'selected=selected':''}}>Individual Seller</option>
									</select>
								</div>
							</div><!-- profile-details -->

							<!-- change-password -->
							<div class="change-password section ">
								<h2>Change password</h2>
								<div class="form-group">
									<label>New password</label>
									<input type="password" name="password" value="{{old('password',$user->password)}}" class="form-control">

								</div>
                                <p class="float-right">Enter only when you need to update your password</p>
							</div><!-- change-password -->

							<!-- preferences-settings -->
							{{--<div class="preferences-settings section">--}}
								{{--<h2>Preferences Settings</h2>--}}
								{{--<!-- checkbox -->--}}
								{{--<div class="checkbox">--}}
									{{--<label><input type="checkbox" name="logged"> Comments are enabled on my ads </label>--}}
									{{--<label><input type="checkbox" name="receive"> I want to receive newsletter.</label>--}}
									{{--<label><input type="checkbox" name="want">I want to receive advice on buying and selling. </label>--}}
								{{--</div><!-- checkbox -->--}}
							{{--</div><!-- preferences-settings -->--}}

                            <button type="submit" class="btn" value="Update Profile"></button>
							<a href="#" class="btn cancle">Cancle</a>
						</div><!-- user-pro-edit -->
					</div><!-- profile -->
					</form>
					@include('users.user_right_sidebar')
				</div><!-- row -->
			</div>
		</div><!-- container -->



@stop
