@extends('trade.master')
@section('content')
<section id="main" class="clearfix  ad-profile-page">
		<div class="container">


<div class="breadcrumb-section">
  <!-- breadcrumb -->
  <ol class="breadcrumb">
    <li><a href="index.html">Home</a></li>
    <li>Ad Post</li>
  </ol><!-- breadcrumb -->
  <h2 class="title">My Profile</h2>
</div><!-- banner -->

<div class="ad-profile section">
  <div class="user-profile">
    <div class="user-images">
      <img src="{{asset('images/user.jpg')}}" alt="User Images" class="img-responsive">
    </div>
    <div class="user">
      <h2>Hello, <a href="{{route('home')}}">{{$user->name}}</a></h2>
      <!--<h5>You last logged in at: 14-01-2016 6:40 AM [ USA time (GMT + 6:00hrs)]</h5>-->
    </div>

    <div class="favorites-user">
      <div class="my-ads">
        <a href="/myads/active">{{$user->postedAds()->count()}}<small>My ADS</small></a>
      </div>
      <div class="favorites">
        <a href="#">{{$user->likes->count()}}<small>Favorites</small></a>
      </div>
    </div>
  </div><!-- user-profile -->

  <ul class="user-menu">
    <li class="active"><a href="{{route('home')}}">Profile</a></li>
    <li><a href="/myads/active">My ads</a></li>
    <li><a href="/myads/likes">Favourite ads</a></li>
    <li><a href="/myads/pending">Pending approval</a></li>
    <li><a onclick="event.preventDefault();document.getElementById('logout_form').submit();">Logout</a></li>
    <form style="display:none;" id='logout_form' action="{{route('logout')}}" method="post">
      {{csrf_field()}}
    </form>
  </ul>
</div><!-- ad-profile -->
@yield('user_content')

</section>


@endsection
