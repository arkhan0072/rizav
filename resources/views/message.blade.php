@extends('trade.master')
@section('content')

<section id="main" class="clearfix published-page">
  <div class="container">
    <div class="row text-center">
      <div class="col-sm-6 col-sm-offset-3">
        <div class="congratulations">
          <i class="fa fa-check-square-o"></i>
          <h2>Congratulations!</h2>
          <h4>Your ad will be Published soon.</h4>
        </div>
      </div>
    </div><!-- row -->
  </div><!-- container -->
</section><!-- main -->

@stop
