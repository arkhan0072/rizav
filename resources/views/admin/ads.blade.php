@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <!-- /.box -->

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="ads_data" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Condition</th>
                            <th>Amount</th>
                            <th>Negotiable</th>
                            <th>Posted By</th>
                            <th>Category</th>
                            <th>City</th>
                            <th>Action</th>
                            <th>Views</th>
                            <th>Featured</th>
                            <th>Created At</th>

                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Type</th>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Condition</th>
                            <th>Amount</th>
                            <th>Negotiable</th>
                            <th>Posted By</th>
                            <th>Category</th>
                            <th>City</th>
                            <th>Action</th>
                            <th>Views</th>
                            <th>Featured</th>
                            <th>Created At</th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
    </div>
@section('page_level_script')
    <script type="text/javascript">
        $('#ads_data').DataTable({
            dom: 'lBftip',
            buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print'],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: {
                url: 'ads/data',
            },
            columns: [
                {data: 'title', Title: 'title', orderable: false},
                {data: 'is_for_sell', Sale: 'is_for_sell', orderable: false},
                {data: 'titleNew', Title: 'titleNew', orderable: false},
                {data: 'is_new', Condition: 'is_new', orderable: false},
                {data: 'amount', Amount: 'amount', orderable: false},
                {data: 'is_negotiable', Negotiable: 'is_negotiable', orderable: false},
                {data: 'user.name', UserName: 'ads.user', orderable: false},
                {data: 'category.name', Category: 'ads.category', orderable: false},
                {data: 'city.name', City: 'ads.city', orderable: false},
                {data: 'is_active', Status: 'is_active', orderable: false},
                {data: 'views_count', Views: 'views_count', orderable:false},
                {data: 'is_featured', Feature: 'is_featured', orderable:false},
                {data: 'created_at', Created: 'created_at', orderable:false},


            ],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": true
                }
            ],

        });
//ads

        $(document).on('click','.btnActivate', function(){
            var controller = $(this);
            var id = controller.data("id");
            $.ajax({
                url: '{{route('admin.adActive')}}',
                type: "GET",
                data:{
                    ad:id
                },
                success: function(data){
                    // toastr.success('User Account Activated');
                    controller.removeClass("btn-primary btnActivate")
                        .addClass("btn-warning btnSuspend");
                    controller.html('<b>DeActivate Ad</b>');
                    event.preventDefault();
                },
                error: function(data){
                    var obj = JSON.parse(data.responseText);
                    var errors = obj.errors;
                    Object.keys(errors).forEach(function(k){
                        console.log(k + ' - ' + errors[k]);
                        toastr.error(errors[k]);
                        event.preventDefault();
                    });
                }
            });
        });

        $(document).on('click','.btnSuspend', function(){
            var controller = $(this);
            var id = controller.data("id");
            $.ajax({
                url: '{{route('admin.adSuspend')}}',
                type: "GET",
                data:{
                    ad:id
                },
                success: function(data){
                    // toastr.success('User Account Suspended');
                    controller.removeClass("btn-warning btnSuspend")
                        .addClass("btn btn-success btnActivate");
                    controller.html('<b>Activate Ad</b>');
                    event.preventDefault();
                },
                error: function(data){
                    var obj = JSON.parse(data.responseText);
                    var errors = obj.errors;
                    Object.keys(errors).forEach(function(k){
                        console.log(k + ' - ' + errors[k]);
                        toastr.error(errors[k]);
                        event.preventDefault();
                    });
                }
            });
        });
//featured ads
        $(document).on('click','.btnfeatured', function(){
            var controller = $(this);
            var id = controller.data("id");
            $.ajax({
                url: '{{route('admin.featured')}}',
                type: "GET",
                data:{
                    ad:id
                },
                success: function(data){
                    // toastr.success('User Account Activated');
                    controller.removeClass("btn-primary btnfeatured")
                        .addClass("btn-warning btnunfeatured");
                    controller.html('<b>UnFeatured this Ad</b>');
                    event.preventDefault();
                },
                error: function(data){
                    var obj = JSON.parse(data.responseText);
                    var errors = obj.errors;
                    Object.keys(errors).forEach(function(k){
                        console.log(k + ' - ' + errors[k]);
                        toastr.error(errors[k]);
                        event.preventDefault();
                    });
                }
            });
        });

        $(document).on('click','.btnunfeatured', function(){
            var controller = $(this);
            var id = controller.data("id");
            $.ajax({
                url: '{{route('admin.unfeatured')}}',
                type: "GET",
                data:{
                    ad:id
                },
                success: function(data){
                    // toastr.success('User Account Suspended');
                    controller.removeClass("btn-warning btnunfeatured")
                        .addClass("btn btn-success btnfeatured");
                    controller.html('<b>Featured this Ad</b>');
                    event.preventDefault();
                },
                error: function(data){
                    var obj = JSON.parse(data.responseText);
                    var errors = obj.errors;
                    Object.keys(errors).forEach(function(k){
                        console.log(k + ' - ' + errors[k]);
                        toastr.error(errors[k]);
                        event.preventDefault();
                    });
                }
            });
        });

    </script>
@endsection
    @stop