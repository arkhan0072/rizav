@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">All Categories</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->

                                    <div class="box-body">

                                        @if(Session::has('message'))
                                        <div class="alert alert-{{Session::has('message') ? 'success':'danger'}} alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-info"></i> Success!</h4>
                                            {{Session::get(message)}}
                                        </div>
                                        @endif



                                        <table id="users_data" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Icon</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>

                                            @foreach($categories as $category)
                                                <tr>
                                                <td>{{$category->name}}</td>
                                                <td><i class="fa {{$category->fa_icon}}" style="font-size:42px;"></i></td>
                                                   <td><a href="{{ route('category.edit', $category->id) }}"><button class="btn btn-warning btnSuspend" data-id="1">Update</button></a> | <input type="submit" class="btn btn-warning btnSuspend" onclick="frmdlt{{$category->id}}.submit();" >
                                                       <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$category->id}}" action="{{ route('category.destroy', $category->id)}}" method="post">
                                                           {!! method_field('delete') !!}
                                                           {{csrf_field()}}

                                                       </form> </td>
                                                </tr>

                                            @endforeach
                                        </table>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="box-footer">

                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
        </section>
    </div>

@stop
