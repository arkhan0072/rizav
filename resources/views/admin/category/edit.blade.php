@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add new Category</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('category.update',$category->id)}}">

                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Category Name" required="required" value="{{$category->name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Icon</label>
                                            <select class="js-source-states">
                                                    <option value="fa-car" data-icon="fa-car" {{$category->fa_icon == 'fa-car' ? 'selected':'' }}>Car</option>
                                                    <option value="fa-desktop" data-icon="fa-desktop" {{$category->fa_icon == 'fa-desktop' ? 'selected':'' }}>Electronics</option>
                                                    <option value="fa-motorcycle" data-icon="fa-motorcycle" {{$category->fa_icon == 'fa-motorcycle' ? 'selected':'' }}>Moter Bike</option>
                                                    <option value="fa-home" data-icon="fa-home" {{$category->fa_icon == 'fa-home' ? 'selected':'' }}>Home</option>
                                                    <option value="fa-futbol-o" data-icon="fa-futbol-o" {{$category->fa_icon == 'fa-futbol-o' ? 'selected':'' }}>Sports</option>
                                                    <option value="fa-paw" data-icon="fa-paw" {{$category->fa_icon == 'fa-paw' ? 'selected':'' }}>Animals</option>
                                                    <option value="fa-gift" data-icon="fa-gift" {{$category->fa_icon == 'fa-gift' ? 'selected':'' }}>Gifts</option>
                                                    <option value="fa-female" data-icon="fa-female" {{$category->fa_icon == 'fa-female' ? 'selected':'' }}>Health & Care</option>
                                                    <option value="fa-linode" data-icon="fa-linode" {{$category->fa_icon == 'fa-linode' ? 'selected':'' }}>Other</option>
                                            </select>
                                            <select class="js-example-templating js-states form-control" name="icon">
                                            </select>
                                            {{--<input type="text" class="form-control" name="icon" placeholder="Icon" required="required" value="{{$category->fa_icon}}">--}}
                                        </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1"></label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="parent">
                                            <option value="0">Parent</option>
                                            @foreach($categories as $cat)
                                                @if($cat->id == $category->parent)
                                                <option value="{{$cat->id}}" selected="selected">{{$cat->name}} </option>
                                                @else
                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-primary" name="btnsubmit" value="Update"> </div>
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
        </section>
    </div>
@section('page_level_style')

    <style>


        /* Optional Wrapper to help with Responsive Designs */
        .select2-wrapper {
            width: 300px;
        }
        /* Optional styling to the right */
        .select2-results .fa {
            float: right;
            position: relative;
            line-height: 20px;
        }
    </style>
@endsection
@section('page_level_script')

    <script type="text/javascript">
        $(document).ready(function() {
            // Initialize "states" example
            var $states = $(".js-source-states");
            var statesOptions = $states.html();
            $states.remove();

            $(".js-states").append(statesOptions);
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        function formatState (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $(
                '<span><i class="fa '+ state.element.dataset.icon.toLowerCase() +'" aria-hidden="true"></i> ' + state.text + '</span>'
            );
            return $state;
        };

        $(".js-example-templating").select2({
            templateResult: formatState
        }).addClass("form-control");
    </script>
@endsection
@stop
