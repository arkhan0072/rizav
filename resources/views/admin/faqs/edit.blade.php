@extends('admin.theme.master')
@section('content')

    <div class="content-wrapper" style="min-height: 901px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>


        <div class="accordion">
            <div class="col-xs-12">

                <div class="panel-group" id="accordion">
                    <form class="" action="{{route('faqs.update',$faq->id)}}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        {{csrf_field()}}
                        <div class="panel panel-default panel-faq">
                            <div class="panel-heading active-faq">
                                <input type="text" name="title" class="form-control" value="{{$faq->title}}">

                            </div><!-- panel-heading -->

                            <div id="faq-one" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    <textarea class="form-control" name="content">{{$faq->content}}</textarea>

                                    <div class="col-xs-12"><br>
                                        <!-- panel -->
                                        <button type="submit" class="btn btn-primary">Create Your FAQ</button>
                                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@stop