@extends('trade.master')
@section('content')
    <section id="main" class="clearfix page">
        <div class="container">
            <div class="faq-page">
                <div class="breadcrumb-section">
                    <!-- breadcrumb -->
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>FAQ</li>
                    </ol><!-- breadcrumb -->
                    <h2 class="title">Trade ads FAQ</h2>
                </div>

                <div class="accordion">
                    <div class="panel-group" id="accordion">
                        @if(Session::has('update'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-info"></i> Success!</h4>
                                FAQ updated Successfully
                            </div>
                        @endif
                        @if(Session::has('deleted'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-info"></i> Success!</h4>
                                FAQ deleted Successfully
                            </div>
                        @endif
                        <div class="panel panel-default panel-faq">
                            @foreach($faqs as $faq)
                                <div class="panel-heading active-faq">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faq-one">
                                        <h4 class="panel-title">
                                            {{$faq->title}}
                                            <span class="pull-right"><i class="fa fa-minus"></i></span>
                                        </h4>
                                    </a>
                                </div><!-- panel-heading -->

                                <div id="faq-one" class="panel-collapse collapse collapse in">
                                    <div class="panel-body">
                                        <p>{{$faq->content}}</p>
                                        <p><a href="{{route('faqs.edit',$faq->id)}}"><button class="btn btn-warning btnSuspend" data-id="1">EDIT</button></a> |<input type="submit" class="btn btn-warning btnSuspend" onclick="frmdlt{{$faq->id}}.submit();" >
                                        <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$faq->id}}" action="{{ route('faqs.destroy', $faq->id)}}" method="post">
                                            {!! method_field('delete') !!}
                                            {{csrf_field()}}

                                        </form>  </p>
                                    </div>
                                </div>
                        </div><!-- panel -->

                        @endforeach
                    </div>
                </div>

            </div><!-- faq-page -->
        </div><!-- container -->
        <section id="something-sell" class="clearfix parallax-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="title">Did Not find your answer yet? Still need help ?</h2>
                        <h4>Send us a note to Help Center</h4>
                        <a href="contact-us.html" class="btn btn-primary">Contact Us</a>
                    </div>
                </div><!-- row -->
            </div><!-- contaioner -->
        </section><!-- something-sell -->
    </section>
@stop