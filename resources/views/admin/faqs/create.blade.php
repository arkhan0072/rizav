@extends('admin.theme.master')
@section('content')

    <div class="content-wrapper" style="min-height: 901px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>


                <div class="accordion">
                    <div class="col-xs-12">

                    <div class="panel-group" id="accordion">
<form class="" action="{{route('faqs.store')}}" method="post">
    {{csrf_field()}}
                        <div class="panel panel-default panel-faq">
                            <div class="panel-heading active-faq">
                                        <input type="text" name="title" class="form-control" placeholder="FAQ TITLE">

                            </div><!-- panel-heading -->

                            <div id="faq-one" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    <textarea class="form-control" name="content"></textarea>

                                <div class="col-xs-12"><br>
                        <!-- panel -->
                                <button type="submit" class="btn btn-primary">Create Your FAQ</button>
                                </div>
</form>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>

@stop