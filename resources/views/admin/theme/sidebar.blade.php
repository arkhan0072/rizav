<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{$user->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-btn">--}}
                {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat">--}}
                  {{--<i class="fa fa-search"></i>--}}
                {{--</button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{route('admin.index')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>

            </li>
           <li>
                <a href="{{route('admin.users')}}">
                    <i class="fa fa-group"></i>
                    <span>Users</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('admin.ads')}}">
                    <i class="fa fa-laptop"></i>
                    <span>Ads</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('category.index')}}">
                    <i class="fa fa-list-alt"></i>
                    <span>Category</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('category.create')}}">
                    <i class="fa fa-edit"></i>
                    <span>Create New Category</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('pages.create')}}">
                    <i class="fa fa-edit"></i>
                    <span>Create New Page</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('faqs.create')}}">
                    <i class="fa fa-edit"></i>
                    <span>Create New Faq</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('states.index')}}">
                    <i class="fa fa-edit"></i>
                    <span>States</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('states.create')}}">
                    <i class="fa fa-edit"></i>
                    <span>Add New State</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('city.index')}}">
                    <i class="fa fa-edit"></i>
                    <span>Cities</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>
            <li>
                <a href="{{route('city.create')}}">
                    <i class="fa fa-edit"></i>
                    <span>Add New City</span>
                    <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
                </a></li>

        </ul>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>