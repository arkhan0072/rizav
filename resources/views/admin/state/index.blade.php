@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">All States</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->

                                    <div class="box-body">

                                        @if(Session::has('update'))
                                        <div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-info"></i> Success!</h4>
                                            State updated Successfully
                                        </div>
                                        @endif
                                            @if(Session::has('deleted'))
                                                <div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <h4><i class="icon fa fa-info"></i> Success!</h4>
                                                    State deleted Successfully
                                                </div>
                                            @endif


                                        <table id="users_data" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Name</th>

                                                <th>Action</th>


                                            </tr>
                                            </thead>

                                            @foreach($state as $states)
                                                <tr>
                                                <td>{{$states->name}}</td>

                                                   <td><a href="{{ route('states.edit', $states->id) }}"><button class="btn btn-warning btnSuspend" data-id="1">Update</button></a> | <input type="submit" class="btn btn-warning btnSuspend" onclick="frmdlt{{$states->id}}.submit();" >
                                                       <form onSubmit="if(!confirm('Is the form filled out correctly?')){return false;}" name="frmdlt{{$states->id}}" action="{{ route('states.destroy', $states->id)}}" method="post">
                                                           {!! method_field('delete') !!}
                                                           {{csrf_field()}}

                                                       </form> </td>
                                                </tr>

                                            @endforeach
                                        </table>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="box-footer">

                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
        </section>
    </div>

@stop
