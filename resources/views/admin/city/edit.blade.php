@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add new Category</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('city.update',$city->id)}}">
                                    <input type="hidden" name="_method" value="PUT">
                                    {{csrf_field()}}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" name="name" placeholder="City Name" required="required" value="{{$city->name}}">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"></label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="state">

                                            @foreach($states as $state)
                                                @if($state->id == $city->state_id)
                                                <option value="{{$state->id}}" selected="selected">{{$state->name}}</option>
                                                @else
                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-primary" name="btnsubmit" value="Add">          </div>
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
        </section>
    </div>

@stop
