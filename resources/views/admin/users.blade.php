@extends('admin.theme.master')
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="users_data" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile Number</th>
                                    <th>Date</th>
                                    <th>Ads Count</th>
                                </tr>
                                </thead>

                                <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile Number</th>
                                    <th>Date</th>
                                    <th>Ads Count</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@section('page_level_script')
    <script type="text/javascript">
        $('#users_data').DataTable({
            dom: 'lBftip',
            buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5', 'print'],
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            serverSide: true,
            ajax: {
                url: 'users/data',
            },
            columns: [
                {data: 'name', name: 'name', orderable: false},
                {data: 'email', name: 'email', orderable: false},
                {data: 'mobile', name: 'mobile', orderable: false},
                {data: 'created_at', name: 'created_at', orderable: false},
                {data: 'posted_ads_count', name: 'posted_ads_count', orderable: false},

            ],
        });
    </script>
    @endsection
@stop
