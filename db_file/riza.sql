-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2018 at 05:21 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riza`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `is_for_sell` tinyint(1) NOT NULL,
  `title` varchar(100) NOT NULL,
  `is_new` tinyint(1) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `is_negotiable` tinyint(1) NOT NULL DEFAULT '0',
  `brand_name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `is_for_sell`, `title`, `is_new`, `amount`, `is_negotiable`, `brand_name`, `description`, `user_id`, `category_id`, `city_id`, `is_active`, `views_count`, `created_at`, `updated_at`) VALUES
(1, 1, 'OPPO1', 1, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 5, 1, 1, 76, '2018-07-12 15:14:11', '2018-07-13 14:49:18'),
(2, 0, 'OPPO4', 1, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 6, 2, 1, 74, '2018-07-12 15:15:15', '2018-07-13 14:00:22'),
(3, 1, 'title', 1, '100.00', 0, 'Sony experia z', 'apoweirywiuer v hwleriqwi eriluweh', 1, 4, 1, 1, 10, '2018-07-12 15:24:34', '2018-07-16 08:26:31'),
(4, 1, 'OPPO2', 1, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 6, 1, 0, 1, '2018-07-12 15:14:11', '2018-07-13 14:58:51'),
(5, 0, 'OPPO', 0, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 4, 2, 0, 0, '2018-07-12 15:15:15', '2018-07-12 15:15:15'),
(6, 1, 'title', 1, '100.00', 0, 'Sony experia z', 'apoweirywiuer v hwleriqwi eriluweh', 1, 4, 1, 1, 0, '2018-07-12 15:24:34', '2018-07-12 15:24:34'),
(7, 1, 'OPPO5', 1, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 5, 1, 0, 1, '2018-07-12 15:14:11', '2018-07-13 14:58:42'),
(8, 0, 'OPPO6', 0, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 4, 2, 0, 0, '2018-07-12 15:15:15', '2018-07-12 15:15:15'),
(9, 1, 'title', 1, '100.00', 0, 'Sony experia z', 'apoweirywiuer v hwleriqwi eriluweh', 1, 4, 1, 1, 1, '2018-07-12 15:24:34', '2018-07-13 14:58:59'),
(10, 1, 'OPPO8', 1, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 5, 1, 0, 0, '2018-07-12 15:14:11', '2018-07-12 15:14:11'),
(11, 0, 'OPPO', 0, '90.00', 1, 'Sony 112', 'YWQOIEROQIENJDKSAJFDHALIUD', 1, 4, 2, 0, 4, '2018-07-12 15:15:15', '2018-07-16 14:04:46'),
(12, 1, 'title', 1, '100.00', 0, 'Sony experia z', 'apoweirywiuer v hwleriqwi eriluweh', 1, 4, 1, 1, 3, '2018-07-12 15:24:34', '2018-07-16 14:04:54');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `icon` varchar(64) NOT NULL,
  `parent` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `icon`, `parent`) VALUES
(1, 'zain', 'khalid', 0),
(2, 'ada', 'khalid', 0),
(3, 'afdfd', 'khalid', 0),
(4, 'child', 'child', 1),
(5, 'child2', 'child2', 2),
(6, 'child3', 'child3', 3);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `state_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `state_id`) VALUES
(1, 'Abu Dhabi', 1),
(2, 'Ajman', 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`) VALUES
(1, 'Abu Dhabi'),
(2, 'Ajman');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_dealer` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `mobile`, `company`, `is_dealer`, `created_at`, `updated_at`) VALUES
(1, 'asdfasdf', 'abdulrehmankhan008@gmail.com', '$2y$10$HmgOr8gQWm45o06TuDJD7uTENs47IZk1jYHOGZsT4hEx5z5dS1iBG', 'x1aTm97I3dTeYCq0mOa07Y30Wqa9UxGOt25emHxajq8KS1KA2kBpKcEFqezG', '787878', NULL, 0, '2018-07-10 05:10:46', '2018-07-10 05:10:46'),
(2, 'asdfasdf', 'abdulrehmankhan0081@gmail.com', '$2y$10$aW4eFmOSi2kZGhJCJSZ8RuZWekOjgrXvUrgttsWT/E8bKsGGFilRu', 'xa5K12iBmJXPTZ1sHXyiWYOnnUi0s9zrPxA2BX6kUNy8BlCSDKO20bivklua', '1234567811', NULL, 0, '2018-07-10 05:39:45', '2018-07-10 05:39:45'),
(3, 'asdfasdf11', 'abdulrehmankhan018@gmail.com', '$2y$10$Zmnu8sRlrl5ENpzlizPmv.PzFGF0UTtMps5eLzUkI8d.XLDneY9PG', 'SZTIUP8W4uzAh1XjuHEMn3LLQvgJp4YvE5ithN1e30qU3R0i5osHa1JcYkYs', '9287392847', NULL, 0, '2018-07-10 05:41:35', '2018-07-10 05:41:35'),
(4, 'asdfasdf', 'abdulrehmankhan000@gmail.com', '$2y$10$BPxHF26oRrObfEJHrPZQ.uH3X8/XHse5FUj3dLQ0Bqq2GR6P9oTna', 'SGtcejJLZWAbS6AAeXuj6tyEN0AsLKN7KVPKcA6IEMoTYx0UwlwevKsa6jAx', '928739284111', NULL, 0, '2018-07-10 05:52:49', '2018-07-10 05:52:49'),
(5, 'asdfasdf111', 'abdulrehmankhan00118@gmail.com', '$2y$10$40Xs4ZZzSgeAiE1uoyO53O8mkOPTUa3EFKZ8o2RrD0s2P1SAllufm', 'WzTMHEN34ueeD9YKKFWvmFyTRCLrPVIFDnpMFFcvznA8A1l4djD1GQ6dyTon', '625161876287', 'private', 0, '2018-07-10 05:57:02', '2018-07-10 05:57:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stateID` (`state_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
