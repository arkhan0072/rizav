<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public function parentCategory(){
      return $this->belongsTo('App\Category','parent');
    }


    public function subCategories(){
      return $this->hasMany('App\Category', 'parent');
    }

    public function ads(){
        return $this->hasMany('App\Ad');
    }


}
