<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Auth;
use Entrust;
use DataTables;
use App\Ad;
use App\User;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $user = Auth::User();
        $categoryCount=Category::count();
        $adActiveCount=Ad::where('is_active',1)->count();
        $adPendingCount=Ad::where('is_active',0)->count();
        if(!$user->hasRole('admin'))
            return redirect()->route('home')->with('baby', 'Baby Hold On! you are not an admin.');
        return view('admin.index', compact('user','adActiveCount','adPendingCount','categoryCount'));

    }
    public function ad(){
        $user = Auth::User();
        return view('admin.ads',compact('user'));
    }
    public function adsData(){
        $ads = Ad::with('user','category','city')->orderBy('id','ASC');
         return DataTables::Eloquent($ads)
             ->addColumn('titleNew', function(Ad $ad){
                return "<a href='/ads/$ad->id' target='ads' >$ad->title</a>";
             })
             ->addColumn('is_featured', function(Ad $ad) {
            if($ad->is_featured)
                return '<button class="btn btn-warning btnunfeatured" data-id="'.$ad->id.'">UnFeatured this Ad</button>';
                 return '<button class="btn btn-success btnfeatured" data-id="'.$ad->id.'">Featured this Ad</button>';
         })
             ->addColumn('is_for_sell', function(Ad $ad) {
                 if($ad->is_for_sell)
                     return "Sell";
                 return "Buy";
             })
             ->addColumn('is_new', function(Ad $ad) {
                 if($ad->is_new)
                     return "New";
                 return "Used";
             })
             ->addColumn('is_negotiable', function(Ad $ad) {
                 if($ad->is_negotiable)
                     return "Yes";
                 return "No";
             })
             ->addColumn('is_active', function(Ad $ad) {
                 if($ad->is_active)
                       return '<button class="btn btn-warning btnSuspend" data-id="'.$ad->id.'">Deactivate Ad</button>';
                 return '<button class="btn btn-success btnActivate" data-id="'.$ad->id.'">Activate Ad</button>';

             })->rawColumns(['is_active','is_featured','titleNew'])

             ->toJson();;
    }

    public function user(){
        $user = Auth::User();
        return view('admin.users',compact('user'));
    }
    public function userData(){
        $ads = User::withCount('postedAds')->get();
        return DataTables::of($ads)->toJson();
    }
    public function adActive(Request $request){
        $ad = Ad::findOrFail($request->ad);
        $ad->is_active = 1;
        $ad->save();
        return 'true';
    }
    public function adSuspend(Request $request){
        $ad = Ad::findOrFail($request->ad);
        $ad->is_active = 0;
        $ad->save();
        return 'true';
    }

    public function Featured(Request $request){
        $ad = Ad::findOrFail($request->ad);
        $ad->is_featured = 1;
        $ad->save();
        return 'true';
    }
    public function unFeatured(Request $request){
        $ad = Ad::findOrFail($request->ad);
        $ad->is_featured = 0;
        $ad->save();
        return 'true';
    }
    public function category(Request $request){
        $user= Auth::User();

        $categories= Category::with([
            'subCategories' => function($query){
                $query->select('id', 'name', 'parent');
            }
        ])->select('id', 'name')->where('parent',0)->get();
        return view('admin/category/create',compact('user','categories'));

    }



}
