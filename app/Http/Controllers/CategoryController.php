<?php

namespace App\Http\Controllers;
use App\User;
use App\Category;
use Auth;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware(function ($request, $next) {
//            $user = Auth::user()->projects;
//            return $next($request);
//        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $user= Auth::User();
        $categories= Category::with([
            'subCategories' => function($query){
                $query->select('id', 'name', 'parent');
            }
        ])->select('id', 'name','fa_icon')->where('parent',0)->get();
        return view('admin/category/index',compact('user','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::User();

        $categories = Category::where('parent', 0)->get();
        return view('admin.category.create', compact('categories','user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'unique:categories|max:64|required',
            'fa_icon' => 'required|max:64',
            'parent' => 'required|max:64',
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->fa_icon = $request->icon;
        $category->parent = $request->parent;
        $category->save();



        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $user=Auth::User();
        $categories = Category::where('parent', 0)->get();
        return view('admin.category.edit',compact('category','categories','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $validatedData = $request->validate([
            'name' => 'max:64|required|unique:categories,name',$category->id,
            'fa_icon' => 'required|max:64',
            'parent' => 'required|max:64',
        ]);
        $category->name = $request->name;
        $category->fa_icon = $request->icon;
        $category->parent = $request->parent;
        $category->save();
        return redirect(route('category.index'))->with(['update'=>'Category successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('category.index'))->with(['deleted'=>'Category successfully updated.']);
    }
}
