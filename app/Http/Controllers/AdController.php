<?php

namespace App\Http\Controllers;
use App\Image;
use Auth;
use App\Ad;
use App\State;
use App\Category;
use Validator;
use Route;
use Illuminate\Routing\UrlGenerator;
use Input;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Facades\File;
class AdController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $ads= Ad::with('category.parentCategory', 'city.state','user','images')
          ->active()
          ->when($request->keyword, function ($q) use ($request){
              $q->where('title', 'like', "%$request->keyword%");
          })
          ->when($request->category, function ($q) use ($request){
              $q->where('category_id', $request->category);
          })
          ->paginate(5);



      $categories = Category::withCount('ads')->where('parent', '<>', 0)->get();

        return view('ads.index',compact('ads','categories'));
    }

    public function likeIt(Ad $ad,Request $request){
        $user = Auth::User();
        if($request->action=='like')
        $user->likes()->syncWithoutDetaching($ad);
       else
           $user->likes()->detach($ad);

        return redirect()->back();

    }
    public function selectCategory(Request $request){
      $user= Auth::User();

      $categories= Category::with([
        'subCategories' => function($query){
          $query->select('id', 'name', 'parent','fa_icon');
      }
      ])
          ->select('id', 'name','fa_icon')->where('parent',0)->get();


      return view('ads.select_category',compact('user','categories'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      try {
        $category = Category::with('parentCategory')
        ->where('id',$request->id)
        ->where('parent', '<>', 0)
        ->firstOrFail();
      } catch (\Exception $e) {
        return redirect('ads/select/category');
      }


        $user =  Auth::User();
        $states = State::all();
          return view('ads.create', compact('user','states','category'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

$user = Auth::User();

        $validatedData = $request->validate([
            'sellType'          => 'required',
            'title'             => 'required|max:64|min:6|string',
            'itemCon'           =>  'required',
            'amount'            => 'required|max:30',
            'brandname'         =>  'required|max:64|min:10',
            'description'       => 'required|max:500|min:100',
//            'image'             =>  'nullable|image'

        ]);
        $ad = new Ad;
        $ad ->is_for_sell=$request->sellType;
        $ad->title = $request->title;
        $ad->is_new=$request->itemCon;
        $ad->amount=$request->amount;
        if($request->has('price'))
        $ad->is_negotiable = $request->price;
        $ad->brand_name= $request->brandname;
        $ad->description = $request->description;
        $ad->user_id = $user->id;
        $ad->category_id= $request->category;
        $ad->city_id=$request->city;
        $ad->save();


        if($request->hasFile('image')) {
            foreach ($request->file('image') as $file) {
                $file_extension = $file->getClientOriginalExtension();
                $file_path = $file->getFilename();
                $filename = $user->id . $ad->id .'_'. str_random(8).'.'.$file_extension;
//                $file->store('uploads/ads');
                Storage::disk('ad_images')->put($filename,File::get($file));
                $image = new Image;
                $image->ad_id = $ad->id;
                $image->name = $filename;
                $image->save();
            }
        }

//            $extension = $file->getClientOriginalExtension();
//            $filename = Auth::id()."_".time().'.'.$extension;
//            Storage::disk('avatars')->put($filename,File::get($file));
//            $user->avatar = $filename;


        return view('message');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
          $ad->views_count=$ad->views_count+1;
          $ad->save();

          $related = Ad::with('category.parentCategory', 'city.state','user','images')->where('category_id', $ad->category_id)->inRandomOrder()->where('id','<>',$ad->id)->take(5)->get();

          return view('ads.show',compact('ad', 'related'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ads)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ads)
    {
        //
    }
}
