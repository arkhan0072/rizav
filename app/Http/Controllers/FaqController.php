<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use Auth;
class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= Auth::User();
        $faqs=Faq::all();
        return view('admin.faqs.index',compact('faqs','user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= Auth::User();
        return view('admin.faqs.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $faq = new Faq;
        $faq->title = $request->title;
        $faq->content = $request->content;
        $faq->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        $user=Auth::User();

        return view('admin.faqs.edit',compact('user','faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $faq->title = $request->title;
        $faq->content=$request->content;
        $faq->save();
        return redirect(route('faqs.index'))->with(['update'=>'FAQ successfully updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {

        $faq->delete();
        return redirect(route('faqs.index'))->with(['deleted'=>'FAQ successfully Deleted.']);

    }
}
