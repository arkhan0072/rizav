<?php

namespace App\Http\Controllers;
use App\Ad;
use App\User;
use App\Category;
use DB;
use Illuminate\Http\Request;
class IndexController extends Controller
{
    public function index(Request $request){



        $adF=Ad::with('images', 'category','user','city.state')
            ->where('is_active','=','1')
            ->where('is_featured','=','1')
            ->limit(5)
            ->get();

        $adP=Ad::with('images', 'category','user','city.state')
            ->where('is_active','=','1')
            ->orderBy('views_count', 'desc')
            ->limit(5)
            ->get();

        $adR=Ad::with('images', 'category','user','city.state')
            ->where('is_active','=','1')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

       $categories = DB::select('Select `categories`.`name`,`categories`.`fa_icon`,`categories`.`id` as DodoBodo, (select count(*) from `ads` inner join `categories` on `categories`.`id` = `ads`.`category_id` where `categories`.`parent` = DodoBodo ) as `ads_count` from `categories` where `parent` = 0');



        return view('index',compact('adF','categories','adP','adR'));
    }
}
