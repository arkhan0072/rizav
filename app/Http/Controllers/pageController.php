<?php

namespace App\Http\Controllers;
use App\Faq;
use Illuminate\Http\Request;

class pageController extends Controller
{
    public function index(){
        //
    }

    public function create(){
        //
    }
    public function store(){
        //
    }
    public function show(){
        //
    }
    public function faq(){
        $faqs=Faq::all();
        return view('page/faq',compact('faqs'));
    }
}
