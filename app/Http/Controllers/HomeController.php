<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Faq;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['faqindex']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        return view('users.home', compact('user'));
    }

    public function myAds(Request $request)
    {
        $user = Auth::User();
        $user->load('likes');
        if ($request->status == 'pending') {
            $ads = $user->postedAds()->with('category.parentCategory', 'images')->pending()->paginate(5);
        }
        if ($request->status == 'active')
            $ads = $user->postedAds()->with('category.parentCategory', 'images')->active()->paginate(5);

        if ($request->status == 'likes')
            $ads = $user->likes()->with('category.parentCategory', 'images')->active()->paginate(5);

        return view('users.myad', compact('user', 'ads'));
    }

    public function fav()
    {

    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required|string|max:191|unique:users,name,' . $user->id,
            'email' => 'required|string|email|max:255|unique:users,email,'. $user->id,
            'mobile' => 'required',
            'is_dealer' => 'required',
            'company' => 'sometimes|required',
            'password' => 'sometimes|min:8',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->is_dealer = $request->is_dealer;
        $user->company = $request->company;
        if ($request->filled('password'))
            $user->password = bcrypt($request->password);
      $user->save();
        return redirect()->back();
    }
    public function faqindex()
    {

        $faqs=Faq::all();
        return view('/faqs',compact('faqs'));

    }
}
