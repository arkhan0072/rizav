<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    public function category(){
      return $this->belongsTo('App\Category');
    }
    public function user(){
      return $this->belongsTo('App\User');
    }
    public function city(){
      return $this->belongsTo('App\City');
    }

    public function scopeActive($query){
        return $query->where('is_active',1)->orderBy('id','DESC');
    }
    public function scopePending($query){
        return $query->where('is_active',0)->orderBy('id','DESC');
    }
    public function images(){
        return $this->hasMany('App\Image');
    }
    public function likedBy(){
        return $this->belongsToMany('App\User','likes');
    }
}
