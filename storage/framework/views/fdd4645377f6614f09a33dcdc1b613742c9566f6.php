<?php $__env->startSection('content'); ?>
<section id="main" class="clearfix  ad-profile-page">
		<div class="container">


<div class="breadcrumb-section">
  <!-- breadcrumb -->
  <ol class="breadcrumb">
    <li><a href="index.html">Home</a></li>
    <li>Ad Post</li>
  </ol><!-- breadcrumb -->
  <h2 class="title">My Profile</h2>
</div><!-- banner -->

<div class="ad-profile section">
  <div class="user-profile">
    <div class="user-images">
      <img src="<?php echo e(asset('images/user.jpg')); ?>" alt="User Images" class="img-responsive">
    </div>
    <div class="user">
      <h2>Hello, <a href="<?php echo e(route('home')); ?>"><?php echo e($user->name); ?></a></h2>
      <!--<h5>You last logged in at: 14-01-2016 6:40 AM [ USA time (GMT + 6:00hrs)]</h5>-->
    </div>

    <div class="favorites-user">
      <div class="my-ads">
        <a href="/myads/active"><?php echo e($user->postedAds()->count()); ?><small>My ADS</small></a>
      </div>
      <div class="favorites">
        <a href="#"><?php echo e($user->likes->count()); ?><small>Favorites</small></a>
      </div>
    </div>
  </div><!-- user-profile -->

  <ul class="user-menu">
    <li class="active"><a href="<?php echo e(route('home')); ?>">Profile</a></li>
    <li><a href="/myads/active">My ads</a></li>
    <li><a href="/myads/likes">Favourite ads</a></li>
    <li><a href="/myads/pending">Pending approval</a></li>
    <li><a onclick="event.preventDefault();document.getElementById('logout_form').submit();">Logout</a></li>
    <form style="display:none;" id='logout_form' action="<?php echo e(route('logout')); ?>" method="post">
      <?php echo e(csrf_field()); ?>

    </form>
  </ul>
</div><!-- ad-profile -->
<?php echo $__env->yieldContent('user_content'); ?>

</section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('trade.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>