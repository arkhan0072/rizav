<?php $__env->startSection('content'); ?>
    <section id="main" class="clearfix page">
        <div class="container">
            <div class="faq-page">
                <div class="breadcrumb-section">
                    <!-- breadcrumb -->
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>FAQ</li>
                    </ol><!-- breadcrumb -->
                    <h2 class="title">Trade ads FAQ</h2>
                </div>

                <div class="accordion">
                    <div class="panel-group" id="accordion">
                        <?php if(Session::has('update')): ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-info"></i> Success!</h4>
                                FAQ updated Successfully
                            </div>
                        <?php endif; ?>
                        <?php if(Session::has('deleted')): ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-info"></i> Success!</h4>
                                FAQ deleted Successfully
                            </div>
                        <?php endif; ?>
                            <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="panel panel-default panel-faq">

                                <div class="panel-heading active-faq">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#faq-<?php echo e($faq->id); ?>">
                                        <h4 class="panel-title">
                                            <?php echo e($faq->title); ?>

                                            <span class="pull-right"><i class="fa fa-minus"></i></span>
                                        </h4>
                                    </a>
                                </div><!-- panel-heading -->

                                <div id="faq-<?php echo e($faq->id); ?>" class="panel-collapse collapse collapse in">
                                    <div class="panel-body">
                                        <p><?php echo e($faq->content); ?></p>
                                    </div>
                                </div>
                        </div><!-- panel -->


                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

            </div><!-- faq-page -->
        </div><!-- container -->
        <section id="something-sell" class="clearfix parallax-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="title">Did Not find your answer yet? Still need help ?</h2>
                        <h4>Send us a note to Help Center</h4>
                        <a href="contact-us.html" class="btn btn-primary">Contact Us</a>
                    </div>
                </div><!-- row -->
            </div><!-- contaioner -->
        </section><!-- something-sell -->
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('trade.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>