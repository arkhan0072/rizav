<?php $__env->startSection('content'); ?>
<section id="main" class="clearfix home-default">
  <div class="container">
    
    <!-- banner -->
    <div class="banner-section text-center">
      <h1 class="title">World's Largest Classifieds Portal </h1>
      <h3>Search from over 15,00,000 classifieds & Post unlimited classifieds free!</h3>
      <!-- banner-form -->
      <div class="banner-form">
        <form action="#">
          <!-- category-change -->
          <div class="dropdown category-dropdown">
            <a data-toggle="dropdown" href="#"><span class="change-text">Select Category</span> <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu category-change">

              <li><a href="#">Fashion & Beauty</a></li>
              <li><a href="#">Cars & Vehicles</a></li>
              <li><a href="#">Electronics & Gedgets</a></li>
              <li><a href="#">Real Estate</a></li>
              <li><a href="#">Sports & Games</a></li>
            </ul>
          </div><!-- category-change -->

          <input type="text" class="form-control" placeholder="Type Your key word">
          <button type="submit" class="form-control" value="Search">Search</button>
        </form>
      </div><!-- banner-form -->

      <!-- banner-socail -->
      <ul class="banner-socail list-inline">
        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>
      </ul><!-- banner-socail -->
    </div><!-- banner -->

    <!-- main-content -->
    <div class="main-content">
      <!-- row -->
      <div class="row">
        <div class="hidden-xs hidden-sm col-md-2 text-center">
          <div class="advertisement">
            <a href="#"><img src="images/ads/2.jpg" alt="images" class="img-responsive"></a>
          </div>
        </div>

        <!-- product-list -->
        <div class="col-md-8">
          <!-- categorys -->
          <div class="section category-ad text-center">
            <ul class="category-list">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="category-item">
                <a href="#">

                  <div class="category-icon"><i class="fa <?php echo e($category->fa_icon); ?>" style="font-size:48px;"></i></div>
                  <span class="category-title"><?php echo e($category->name); ?></span>
                  <span class="category-quantity">(<?php echo e($category->ads_count); ?>)</span>
    </a>
              </li><!-- category-item -->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


            </ul>
          </div><!-- category-ad -->

          <!-- featureds -->

          <div class="section featureds">
            <div class="row">
              <div class="col-sm-12">
                <div class="section-title featured-top">
                  <h4>featured ads</h4>
                </div>
              </div>
            </div>

            <!-- featured-slider -->
            <div class="featured-slider">
              <div id="featured-slider" >
                <!-- featured -->
                  <?php $__currentLoopData = $adF; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="featured">

                  <div class="featured-image">
                    <a href="<?php echo e(route('ads.show',$adf->id)); ?>"><img src="<?php echo e(asset('uploads/ads')); ?>/<?php echo e(isset($adf->images[0]->name) ? $adf->images[0]->name : 'default.png'); ?>" alt="" class="img-respocive"></a>
                    <a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="verified"><i class="fa fa-check-square-o"></i></a>
                  </div>

                  <!-- ad-info -->
                  <div class="ad-info">
                    <h3 class="item-price"><?php echo e($adf->amount); ?></h3>
                    <h4 class="item-title"><a href="<?php echo e(route('ads.show',$adf->id)); ?>"><?php echo e($adf->title); ?></a></h4>
                    <div class="item-cat">
                      <span><a href="#"><?php echo e($adf->category->name); ?></a></span>
                    </div>
                  </div><!-- ad-info -->

                  <!-- ad-meta -->
                  <div class="ad-meta">
                    <div class="meta-content">
                      <span class="dated"><a href="#"><?php echo e($adf->created_at->todatestring()); ?></a></span>
                    </div>
                    <!-- item-info-right -->
                    <div class="user-option pull-right">

                      <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo e($adf->city->name); ?>

                              ,<?php echo e($adf->city->state->name); ?>"><i class="fa fa-map-marker"></i> </a>
                      <a href="#" data-toggle="tooltip" data-placement="top" title="dealer"><i class="fa <?php echo e($adf->user->is_dealer ? 'fa-suitcase' : 'fa-user'); ?>"></i> </a>
                    </div><!-- item-info-right -->
                  </div><!-- ad-meta -->

                </div><!-- featured -->

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div><!-- featured-slider -->
            </div><!-- #featured-slider -->
          </div><!-- featureds -->

          <!-- ad-section -->
          <div class="ad-section text-center">
            <a href="#"><img src="images/ads/3.jpg" alt="image" class="img-responsive"></a>
          </div><!-- ad-section -->

          <!-- trending-ads -->
          <div class="section trending-ads">
            <div class="section-title tab-manu">
              <h4>trending ads</h4>
               <!-- nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#recent-ads"  data-toggle="tab">recent ads</a></li>
                <li role="presentation"><a href="#popular" data-toggle="tab">popular ads</a></li>
              </ul>
            </div>

              <!-- tab panes -->
            <div class="tab-content">
              <!-- tab-pane -->
              <div role="tabpanel" class="tab-pane fade in active" id="recent-ads">
                <!-- ad-item -->
                  <?php $__currentLoopData = $adR; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="ad-item row">
                  <!-- item-image -->
                  <div class="item-image-box col-sm-4">
                    <div class="item-image">
                      <a href="<?php echo e(route('ads.show',$adr->id)); ?>"><img src="<?php echo e(asset('uploads/ads')); ?>/<?php echo e(isset($adr->images[0]->name) ? $adr->images[0]->name : 'default.png'); ?>" alt="image" class="img-responsive"></a>
                      
                    </div><!-- item-image -->
                  </div>

                  <!-- rending-text -->
                  <div class="item-info col-sm-8">
                    <!-- ad-info -->
                    <div class="ad-info">
                      <h3 class="item-price"><?php echo e($adr->amount); ?></h3>
                      <h4 class="item-title"><a href="<?php echo e(route('ads.show',$adr->id)); ?>"><?php echo e($adr->title); ?></a></h4>
                      <div class="item-cat">
                        <span><a href="#"><?php echo e($adr->category->name); ?></a></span>

                      </div>
                    </div><!-- ad-info -->

                    <!-- ad-meta -->
                    <div class="ad-meta">
                      <div class="meta-content">
                        <span class="dated"><a href="#"><?php echo e($adr->created_at->todatestring()); ?></a></span>
                        <a href="#" class="tag"><i class="fa <?php echo e($adr->is_new ? 'fa-check':'fa-tag'); ?>"></i><?php echo e($adr->is_new ? 'NEW':'USED'); ?></a>
                      </div>
                      <!-- item-info-right -->
                      <div class="user-option pull-right">
                          <a href="#" data-toggle="tooltip" data-placement="top" class="fa fa-eye"><?php echo e($adr->views_count); ?></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo e($adr->city->name); ?>

                                ,<?php echo e($adr->city->state->name); ?>"><i class="fa fa-map-marker"></i> </a>
                        <a class="online" href="#" data-toggle="tooltip" data-placement="top" title="<?php echo e($adr->user->is_dealer ? 'Dealer':'User'); ?>"><i class="fa <?php echo e($adr->user->is_dealer ? 'fa-suitcase' : 'fa-user'); ?>"></i> </a>
                      </div><!-- item-info-right -->
                    </div><!-- ad-meta -->
                  </div><!-- item-info -->
                </div><!-- ad-item -->

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div><!-- tab-pane -->

              <!-- tab-pane -->
              <div role="tabpanel" class="tab-pane fade" id="popular">
                  <?php $__currentLoopData = $adP; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="ad-item row">
                  <!-- item-image -->
                  <div class="item-image-box col-sm-4">
                    <div class="item-image">
                      <a href="<?php echo e(route('ads.show',$adp->id)); ?>"><img src="<?php echo e(asset('uploads/ads')); ?>/<?php echo e(isset($adp->images[0]->name) ? $adp->images[0]->name : 'default.png'); ?>" alt="image" class="img-responsive"></a>
                      
                    </div><!-- item-image -->
                  </div>

                  <!-- rending-text -->
                  <div class="item-info col-sm-8">
                    <!-- ad-info -->
                    <div class="ad-info">
                      <h3 class="item-price"><?php echo e($adp->amount); ?></h3>
                      <h4 class="item-title"><a href="<?php echo e(route('ads.show',$adp->id)); ?>"><?php echo e($adp->title); ?></a></h4>
                      <div class="item-cat">
                        <span><a href="#"><?php echo e($adp->category->name); ?></a></span>
                      </div>
                    </div><!-- ad-info -->

                    <!-- ad-meta -->
                    <div class="ad-meta">
                      <div class="meta-content">
                        <span class="dated"><a href="#"><?php echo e($adp->created_at->todatestring()); ?></a></span>
                        <a href="#" class="tag"><i class="fa fa-tags"></i> used</a>
                      </div>
                      <!-- item-info-right -->
                      <div class="user-option pull-right">
                          <a href="#" data-toggle="tooltip" data-placement="top" class="fa fa-eye"><?php echo e($adp->views_count); ?></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="<?php echo e($adp->city->name); ?>

                                ,<?php echo e($adp->city->state->name); ?>"><i class="fa fa-map-marker"></i> </a>
                        <a class="online" href="#" data-toggle="tooltip" data-placement="top" title="<?php echo e($adp->user->is_dealer ? 'Dealer':'User'); ?>"><i class="fa <?php echo e($adp->user->is_dealer ? 'fa-suitcase' : 'fa-user'); ?>"></i> </a>
                      </div><!-- item-info-right -->
                    </div><!-- ad-meta -->
                  </div><!-- item-info -->
                </div><!-- ad-item -->

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div><!-- tab-pane -->

              <!-- tab-pane -->

            </div>
          </div><!-- trending-ads -->

          <!-- cta -->
          <div class="section cta text-center">
            <div class="row">
              <!-- single-cta -->
              <div class="col-sm-4">
                <div class="single-cta">
                  <!-- cta-icon -->
                  <div class="cta-icon icon-secure">
                    <img src="images/icon/13.png" alt="icon" class="img-responsive">
                  </div><!-- cta-icon -->

                  <h4>secure trading</h4>
                  <p>duis autem vel eum iriure dolor in hendrerit in</p>
                </div>
              </div><!-- single-cta -->

              <!-- single-cta -->
              <div class="col-sm-4">
                <div class="single-cta">
                  <!-- cta-icon -->
                  <div class="cta-icon icon-support">
                    <img src="images/icon/14.png" alt="icon" class="img-responsive">
                  </div><!-- cta-icon -->

                  <h4>24/7 support</h4>
                  <p>duis autem vel eum iriure dolor in hendrerit in</p>
                </div>
              </div><!-- single-cta -->

              <!-- single-cta -->
              <div class="col-sm-4">
                <div class="single-cta">
                  <!-- cta-icon -->
                  <div class="cta-icon icon-trading">
                    <img src="images/icon/15.png" alt="icon" class="img-responsive">
                  </div><!-- cta-icon -->

                  <h4>easy trading</h4>
                  <p>duis autem vel eum iriure dolor in hendrerit in</p>
                </div>
              </div><!-- single-cta -->
            </div><!-- row -->
          </div><!-- cta -->
        </div><!-- product-list -->

        <!-- advertisement -->
        <div class="hidden-xs hidden-sm col-md-2">
          <div class="advertisement text-center">
            <a href="#"><img src="images/ads/1.jpg" alt="images" class="img-responsive"></a>
          </div>
        </div><!-- advertisement -->
      </div><!-- row -->
    </div><!-- main-content -->
  </div><!-- container -->
</section>

<!-- download -->
<section id="download" class="clearfix parallax-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2>Download on App Store</h2>
      </div>
    </div><!-- row -->

    <!-- row -->
    <div class="row">
      <!-- download-app -->
      <div class="col-sm-4">
        <a href="#" class="download-app">
          <img src="images/icon/16.png" alt="Image" class="img-responsive">
          <span class="pull-left">
            <span>available on</span>
            <strong>Google Play</strong>
          </span>
        </a>
      </div><!-- download-app -->

      <!-- download-app -->
      <div class="col-sm-4">
        <a href="#" class="download-app">
          <img src="images/icon/17.png" alt="Image" class="img-responsive">
          <span class="pull-left">
            <span>available on</span>
            <strong>App Store</strong>
          </span>
        </a>
      </div><!-- download-app -->

      <!-- download-app -->
      <div class="col-sm-4">
        <a href="#" class="download-app">
          <img src="images/icon/18.png" alt="Image" class="img-responsive">
          <span class="pull-left">
            <span>available on</span>
            <strong>Windows Store</strong>
          </span>
        </a>
      </div><!-- download-app -->
    </div><!-- row -->
  </div><!-- contaioner -->
</section><!-- download -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('trade.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>