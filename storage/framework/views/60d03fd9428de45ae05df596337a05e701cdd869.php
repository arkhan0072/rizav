<?php $__env->startSection('user_content'); ?>

			<div class="profile">
				<div class="row">
					<div class="col-sm-8">
						<div class="my-ads section">
							<h2>My ads</h2>
							<!-- ad-item -->
							<?php $__currentLoopData = $ads; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ad): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="ad-item row">
								<!-- item-image -->
								<div class="item-image-box col-sm-4">
									<a href="<?php echo e(route('ads.show',$ad->id)); ?>"><img src="<?php echo e(asset('uploads/ads')); ?>/<?php echo e(isset($ad->images[0]->name) ? $ad->images[0]->name : 'default.png'); ?>" alt="Image"  class="img-responsive"></a>
									<div class="item-image">
									</div><!-- item-image -->
								</div>
								<!-- rending-text -->
								<div class="item-info col-sm-8">
									<!-- ad-info -->
									<div class="ad-info">
										<h3 class="item-price"><?php echo e($ad->amount); ?></h3>
										<h4 class="item-title"><a href="<?php echo e(route('ads.show',$ad->id)); ?>"><?php echo e($ad->title); ?></a></h4>
										<div class="item-cat">
											<span><a href="#"><?php echo e($ad->category->parentCategory->name); ?></a></span> /
											<span><a href="#"><?php echo e($ad->category->name); ?></a></span>
										</div>
									</div><!-- ad-info -->
									<!-- ad-meta -->
									<div class="ad-meta">
										<div class="meta-content">
											<span class="dated">Posted On: <a href="#"> <?php echo e($ad->created_at); ?></a></span>
											<span class="visitors">Views: <?php echo e($ad->views_count); ?></span>
										</div>
										<!-- item-info-right -->
										<div class="user-option pull-right">
											<a class="edit-item" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit this ad"><i class="fa fa-pencil"></i></a>
											<a class="delete-item" href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete this ad"><i class="fa fa-times"></i></a>
										</div><!-- item-info-right -->
									</div><!-- ad-meta -->
								</div><!-- item-info -->
							</div><!-- ad-item -->
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<div class="text-center">
              <?php echo e($ads->links()); ?>

							</div>
						</div>
					</div>
					<?php echo $__env->make('users.user_right_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div><!-- row -->
			</div>
		</div><!-- container -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('users.usermaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>