<?php $__env->startSection('content'); ?>
<!-- signin-page -->
<section id="main" class="clearfix user-page">
  <div class="container">
    <div class="row text-center">
      <!-- user-login -->
      <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="user-account">
          <h2>User Login</h2>
          <!-- form -->
          <form action="#" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-group<?php echo e($errors->has('email') ? 'has-error': ''); ?>">
              <input type="text" class="form-control" name="email" placeholder="email" >
              <?php if($errors->has('email')): ?>
              <span class="help-block">
          <strong><?php echo e($errors->first('email')); ?></strong>
        </span>
              <?php endif; ?>
            </div>
            <div class="form-group<?php echo e($errors->has('password') ? 'has-error': ''); ?>">
              <input type="password" class="form-control" name="password" placeholder="Password" >
              <?php if($errors->has('password')): ?>
              <span class="help-block">
          <strong><?php echo e($errors->first('password')); ?></strong>
        </span>
              <?php endif; ?>
            </div>
            <button type="submit" href="#" class="btn">Login</button>
          </form><!-- form -->

          <!-- forgot-password -->
          <div class="user-option">
            <div class="checkbox pull-left">
              <label for="logged"><input type="checkbox" name="logged" id="logged"> Keep me logged in </label>
            </div>
            <div class="pull-right forgot-password">
              <a href="<?php echo e('password/reset'); ?>">Forgot password</a>
            </div>
          </div><!-- forgot-password -->
        </div>
        <a href="<?php echo e(route('register')); ?>" class="btn-primary">Create a New Account</a>
      </div><!-- user-login -->
    </div><!-- row -->
  </div><!-- container -->
</section><!-- signin-page -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('trade.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>