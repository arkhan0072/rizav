<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Riza</title>

   <!-- CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>" >
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('css/icofont.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/slidr.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
	<link id="preset" rel="stylesheet" href="<?php echo e(asset('css/presets/preset1.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/responsive.css')); ?>">

	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo e(asset('images/ico/apple-touch-icon-144-precomposed.png')); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo e(asset('images/ico/apple-touch-icon-114-precomposed.png')); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo e(asset('images/ico/apple-touch-icon-72-precomposed.png')); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo e(asset('images/ico/apple-touch-icon-57-precomposed.png')); ?>">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- inlcude header -->
<?php echo $__env->make('trade.include.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- main -->
  <?php echo $__env->yieldContent('content'); ?>
	<!-- main -->
	<!--inlcude footer-->
<?php echo $__env->make('trade.include.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- JS -->
    <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/modernizr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo e(asset('js/gmaps.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/smoothscroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/scrollup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/price-range.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.countdown.js')); ?>"></script>
    <script src="<?php echo e(asset('js/custom.js')); ?>"></script>
	<script src="<?php echo e(asset('js/switcher.js')); ?>"></script>
  </body>
</html>
