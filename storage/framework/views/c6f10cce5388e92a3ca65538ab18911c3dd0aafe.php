<?php $__env->startSection('user_content'); ?>
	<?php if(\Session::has('baby')): ?>
		<div class="alert alert-success">
			<ul>
				<li><?php echo \Session::get('baby'); ?></li>
			</ul>
		</div>
	<?php endif; ?>

			<div class="profile">
				<div class="row">
					<div class="col-sm-8">
						<div class="user-pro-section">
							<!-- profile-details -->
							<div class="profile-details section">
								<h2>Profile Details</h2>
								<!-- form -->
								<form action="<?php echo e(route('updateprofile',$user->id)); ?>" method="post">
									<?php echo csrf_field(); ?>
                                    <?php
                                    print_r($errors);
                                    ?>
						<div class="form-group<?php echo e($errors->has('name') ? 'has-error': ''); ?>">
									<label>Username</label>
									<input type="text" name="name" class="form-control" style="color:black;" value="<?php echo e(old('name',$user->name)); ?>">
                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
					<strong><?php echo e($errors->first('name')); ?></strong>
				</span>
                                    <?php endif; ?>
								</div>
								<div class="form-group<?php echo e($errors->has('email') ? 'has-error': ''); ?>">
									<label>Email ID</label>
									<input type="email" name="email" class="form-control" value="<?php echo e(old('email',$user->email)); ?>">
                                    <?php if($errors->has('email')): ?>
                                        <span class="help-block">
					<strong><?php echo e($errors->first('email')); ?></strong>
				</span>
                                    <?php endif; ?>
								</div>

								<div class="form-group<?php echo e($errors->has('mobile') ? 'has-error': ''); ?>">
									<label for="name-three">Mobile</label>
									<input type="text" name="mobile" class="form-control" value="<?php echo e(old('mobile',$user->mobile)); ?>">
                                    <?php if($errors->has('mobile')): ?>
                                        <span class="help-block">
          <strong><?php echo e($errors->first('mobile')); ?></strong>
        </span>
                                    <?php endif; ?>
								</div>
									<div class="form-group<?php echo e($errors->has('company') ? 'has-error': ''); ?>">

											<label for="name-three">Company</label>
										<input type="text" name="company" class="form-control" value="<?php echo e(old('company',$user->company)); ?>" placeholder="company">
                                        <?php if($errors->has('company')): ?>
                                            <span class="help-block">
          <strong><?php echo e($errors->first('company')); ?></strong>
        </span>
                                        <?php endif; ?>
									</div>
								<!--<div class="form-group">
									<label>Your City</label>
									<select class="form-control">
										<option value="#">Los Angeles, USA</option>
										<option value="#">Dhaka, BD</option>
										<option value="#">Shanghai</option>
										<option value="#">Karachi</option>
										<option value="#">Beijing</option>
										<option value="#">Lagos</option>
										<option value="#">Delhi</option>
										<option value="#">Tianjin</option>
										<option value="#">Rio de Janeiro</option>
									</select>
								</div>-->

								<div class="form-group">
									<label>You are a</label>
									<select class="form-control" name="is_dealer">
										<option value="1" <?php echo e($user->is_dealer ?'selected=selected':''); ?>>Dealer</option>
										<option value="0" <?php echo e(!$user->is_dealer ?'selected=selected':''); ?>>Individual Seller</option>
									</select>
								</div>
							</div><!-- profile-details -->

							<!-- change-password -->
							<div class="change-password section ">
								<h2>Change password</h2>
								<div class="form-group">
									<label>New password</label>
									<input type="password" name="password" value="<?php echo e(old('password',$user->password)); ?>" class="form-control">

								</div>
                                <p class="float-right">Enter only when you need to update your password</p>
							</div><!-- change-password -->

							<!-- preferences-settings -->
							
								
								
								
									
									
									
								
							

                            <button type="submit" class="btn" value="Update Profile"></button>
							<a href="#" class="btn cancle">Cancle</a>
						</div><!-- user-pro-edit -->
					</div><!-- profile -->
					</form>
					<?php echo $__env->make('users.user_right_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div><!-- row -->
			</div>
		</div><!-- container -->



<?php $__env->stopSection(); ?>

<?php echo $__env->make('users.usermaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>