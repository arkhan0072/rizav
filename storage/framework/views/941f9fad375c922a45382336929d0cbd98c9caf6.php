<?php $__env->startSection('content'); ?>

<section id="main" class="clearfix ad-post-page">
		<div class="container">

			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo e(route('index')); ?>">Home</a></li>
					<li>Ad Post</li>
				</ol><!-- breadcrumb -->
				<h2 class="title">Post Free Ad</h2>
			</div><!-- banner -->



			<div id="ad-post">
				<div class="row category-tab">
					<div class="col-md-4 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Select a Category</h4>
							<ul role="tablist">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="#cat<?php echo e($value->id); ?>" aria-controls="cat<?php echo e($value->id); ?>" role="tab" data-toggle="tab">

					  <i class="fa <?php echo e($value->fa_icon); ?>" style="font-size:28px; color: green !important;"></i>

                  <?php echo e($value->name); ?>

                </a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

							</ul>
						</div>
					</div>

					<!-- Tab panes -->
					<div class="col-md-4 col-sm-6">
						<div class="section tab-content subcategory post-option">
							<h4>Select a subcategory</h4>
<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div role="tabpanel" class="tab-pane" id="cat<?php echo e($value->id); ?>">
  <ul>
<?php $__currentLoopData = $value->subCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<li><a href="<?php echo e(route('ads.create')); ?>?id=<?php echo e($sub->id); ?>"><i class="fa <?php echo e($sub->fa_icon); ?>" style="font-size:28px; color: green !important;"></i> <?php echo e($sub->name); ?>

		</a></li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="section next-stap post-option">
							<h2>Post an Ad in just <span>30 seconds</span></h2>
							<p>Please DO NOT post multiple ads for the same items or service. All duplicate, spam and wrongly categorized ads will be deleted.</p>
							<div class="btn-section">
								<a href="ad-post-details.html" class="btn">Next</a>
								<a href="#" class="btn-info">or Cancle</a>
							</div>
						</div>
					</div><!-- next-stap -->
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<div class="ad-section">
							<a href="#"><img src="<?php echo e(asset('images/ads/3.jpg')); ?>" alt="Image" class="img-responsive"></a>
						</div>
					</div>
				</div><!-- row -->
			</div>
		</div><!-- container -->
	</section>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('trade.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>